package com.project.dao;

/**
 * Interface for SchemaHadler class
 * */
public interface DaoSchemaHandler {

	public boolean createTable( String tableName );
	public boolean deleteTable( String tableName );
	public boolean deleteAllTables();
	
	public void purgeTable( String tableName );
	public void purgeAllTable();
	
	public boolean createSchema( String schemaName );
	public boolean deleteSchema( String schemaName );
	
	public void initializeDefaultTables( String schemaName );
	
}
