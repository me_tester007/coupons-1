package com.project.dao;

import com.project.daofile.LogLevel;
import com.project.logs.Log;

//	CURRENTLY DEPRICATED AND IS NOT IN USE
// Logs are logged into FILES
/**
 * Inteface for Log DAO object
 * */
public interface LogDao {

	public void createLog( Log log, LogLevel logLevel );
	public void updateLog( Log log );
	public void deleteLog( Log log );
	public int getLogLatestID();
	
}
