package com.project.dao;

import java.util.Collection;

import com.project.beans.Inbox;

/**
 * DAO interface for Inbox DAO object
 * */
public interface InboxDao {

	public void createInboxMessage( Inbox inbox );
	public void removeInboxMessage( Inbox inbox );
	public void updateInboxMessage( Inbox inbox );
	public Collection<Inbox> getAllMessages();
	public Inbox getInboxMessage( int id );
	public void assignInboxToOwner( int owner, int inboxID, String table );
	public void removeInboxFromOwner(int inboxID, String table);
	public void removeInboxOwner(int ownerID, String colName, String table);
	public int getInboxLatestID();
	
}
