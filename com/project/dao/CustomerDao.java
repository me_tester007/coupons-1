package com.project.dao;

import java.util.Collection;

import com.project.beans.Coupon;
import com.project.beans.Customer;
import com.project.beans.Inbox;

/**
 * DAO interface for Customer DAO object
 * */
public interface CustomerDao {

	public void createCustomer( Customer c );
	public void removeCustomer( Customer c );
	public void updateCustomer( Customer c );
	public Customer getCustomer( int id );
	public Customer getCustomer( String name );
	public Collection<Customer> getAllCustomers();
	public Collection<Coupon> getCoupons( int custID );
	public Collection<Inbox> getInboxMessages( int custID );
	public boolean login( String custName, String pwd );
	public int getCustomerLatestID();
	
}
