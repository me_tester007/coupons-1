package com.project.daofile;

/**
 * DAO layer that writes log objects into the file 
 * */
import java.io.FileWriter;
import java.io.IOException;

import com.project.dao.LogDao;
import com.project.exceptions.file.FileRunTimeException;
import com.project.logs.Log;

public class LogFileDao implements LogDao {
	
	private static final LogFileDao _fileDao = new LogFileDao();
	
	
	private LogFileDao() {}
	
	public static LogFileDao getInstance() {
		return _fileDao;
	}
	
	public void createSpecificLog(Log log, LogType type, LogLevel logLevel) throws FileRunTimeException {

		FileWriter _writer = null;
		LogFileConnection connection = null;
		
		switch ( type ) {
		case ALL:
			connection = LogFileConnection.getLogAllInstance(); break;
		case COMPANY:
			connection = LogFileConnection.getLogCompanyInstance(); break;
		case CUSTOMER:
			connection = LogFileConnection.getLogCustomerInstance(); break;
		case COUPON:
			connection = LogFileConnection.getLogCouponInstance(); break;
		case INBOX:
			connection = LogFileConnection.getLogInboxInstance(); break;
		case UNDEFINED: break;
		default:
			connection = LogFileConnection.getLogAllInstance(); break;
		}
		
		if ( connection == null )
			return;

		_writer = connection.getConnection();
		try {
			_writer.write(log.getData(logLevel) + "\n");
		} catch (IOException e) {
			throw new FileRunTimeException(e, FileRunTimeException.IO_ERR);
		} finally {
			connection.returnConnection(_writer);
		}
	}
	
	@Override
	public void createLog( Log log, LogLevel logLevel ) throws FileRunTimeException {
		
		FileWriter _writer = null;
		LogFileConnection _connection = LogFileConnection.getLogAllInstance();
		if ( _connection == null )
			return;
		
		_writer = _connection.getConnection();
		try {
			_writer.write(log.getData(logLevel) + "\n");
		} catch (IOException e) {
			throw new FileRunTimeException(e, FileRunTimeException.IO_ERR);
		} finally {
			_connection.returnConnection(_writer);
		}
	}

	// NOT IN USE
	@Override
	public void updateLog(Log log) throws FileRunTimeException {
		// DO NOTHING
		return;
	}

	// NOT IN USE
	@Override
	public void deleteLog(Log log) throws FileRunTimeException {
		// DO NOTHING
		return;
	}

	// NOT IN USE
	@Override
	public int getLogLatestID() throws FileRunTimeException {
		// DO NOTHING
		return 1;
	}

	 
}
