package com.project.daofile;

/**
 * Class handles Log File Connections
 * sort of file connection pool
 * */
import java.io.FileWriter;
import java.io.IOException;

import com.project.constants.FileNames;
import com.project.exceptions.GenRunTimeException;
import com.project.exceptions.file.FileRunTimeException;
import com.project.utils.FileExceptionUtils;

public class LogFileConnection {

	private static LogFileConnection _logAll = null;
	private static LogFileConnection _logCompany = null;
	private static LogFileConnection _logCustomer = null;
	private static LogFileConnection _logCoupon = null;
	private static LogFileConnection _logInbox = null;
	private FileWriter _writer;
	private boolean _availableConnection = true;
	
	static {
		try {
			_logAll = new LogFileConnection(FileNames.COUPONSYS_ALL);
			_logCompany = new LogFileConnection(FileNames.COMPANY);
			_logCustomer = new LogFileConnection(FileNames.CUSTOMER);
			_logCoupon = new LogFileConnection(FileNames.COUPON);
			_logInbox = new LogFileConnection(FileNames.INBOX);
		} catch (FileRunTimeException e) {
			FileExceptionUtils.fileException(e);
		}
	}
	
	private LogFileConnection( final String fileName ) throws FileRunTimeException {	
		try {
			_writer = new FileWriter(fileName, true);
		} catch (IOException e) {
			throw new FileRunTimeException(e, FileRunTimeException.IO_ERR);
		}
	}
	
	public static LogFileConnection getLogAllInstance() {
		return _logAll;
	}
	
	public static LogFileConnection getLogCompanyInstance() {
		return _logCompany;
	}
	
	public static LogFileConnection getLogCustomerInstance() {
		return _logCustomer;
	}
	
	public static LogFileConnection getLogCouponInstance() {
		return _logCoupon;
	}
	
	public static LogFileConnection getLogInboxInstance() {
		return _logInbox;
	}
	
	public synchronized FileWriter getConnection() throws GenRunTimeException {
		if ( ! _availableConnection )
			try {
				wait();
			} catch (IllegalMonitorStateException e) {
				throw new GenRunTimeException(e, GenRunTimeException.ILLEGAL_MON);
			} catch (InterruptedException e) {
				throw new GenRunTimeException(e, GenRunTimeException.INTERRUPTED);
			}
		
		_availableConnection = false;
		return _writer;
	}
	
	public synchronized void returnConnection( FileWriter writer ) throws FileRunTimeException {
		try {
			writer.flush();
		} catch (IOException e) {
			throw new FileRunTimeException(e, FileRunTimeException.IO_ERR);
		}
		_availableConnection = true;
		notifyAll();
	}
	
	public synchronized void closeFileConnection() throws FileRunTimeException {
		try {
			_writer.close();
		} catch (IOException e) {
			throw new FileRunTimeException(e, FileRunTimeException.IO_ERR);
		}
	}
	
	public static void closeAllFilesConnections() throws FileRunTimeException {
		_logAll.closeFileConnection();
		_logCompany.closeFileConnection();
		_logCoupon.closeFileConnection();
		_logCustomer.closeFileConnection();
		_logInbox.closeFileConnection();
	}
	
}
