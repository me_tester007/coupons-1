package com.project.tasks;

import java.sql.Date;
import java.util.Collection;

import com.project.beans.Coupon;
import com.project.beans.Inbox;
import com.project.constants.SqlDB;
import com.project.dao.CouponDao;
import com.project.daofile.LogLevel;
import com.project.daofile.LogType;
import com.project.exceptions.sql.CouponSqlException;
import com.project.logs.Logger;
import com.project.utils.ExceptionUtils;
import com.project.utils.FactoryDao;
import com.project.utils.InboxUtils;
import com.project.utils.SqlExceptionUtils;

/**
 * Class is a Thread that will run once in 24 hours<br/>
 * It searches for expired coupons and removes them<br/>
 * */
public class DayliCouponExpirationTask implements Runnable {
	
	private CouponDao _couponDao;
	Collection<Coupon> coupons;
	private boolean _quite = false;
	
	public  DayliCouponExpirationTask() {}
	
	/**
	 * Runnable Run method - every 24 hours awakes requests all available Coupons from DB<br/>
	 * and verifies if Coupon is expired following by removing it<br/>
	 * @return void
	 * */
	@Override
	public void run() {
		while ( ! _quite ) {
			try {
					_couponDao = FactoryDao.getCouponDao();
				if ( (coupons = _couponDao.getAllCoupons()) != null )
					verifyCouponsExpireDate( coupons );
			} catch (CouponSqlException e) {
				SqlExceptionUtils.couponException(e);
			} finally {
				try {
					//Thread.sleep(86400000);
					Thread.sleep(20000);
				} catch (InterruptedException e) {
					ExceptionUtils.dayliTaskInterrupted(e);
				}
			}
		}
	}
	
	/*
	 * Creates Date 'current' object then scans through Coupons Collection and compares it<br/>
	 * to the specific coupon's getEndDate() variable<br/>
	 * in case current Date object is later/after than the coupon's Date
	 * it calls to {@linkplain RemoveCouponFromDB} function which removes the coupon<br/>
	 * @param Collection
	 * */
	private void verifyCouponsExpireDate( Collection<Coupon> coupons ) {
		Date currentDate = new Date(new java.util.Date().getTime());
		for ( Coupon coupon : coupons )
			if ( coupon.getEndDate().before(currentDate) ) {
				System.out.println("\tFound expired Coupon");
				removeCouponFromDB(coupon);
			}
	}
	
	/*
	 * Gets the specific coupon and removes it from all join tables followed by removing it from<br/>
	 * coupon's table itself
	 * @param coupon
	 * */
	private void removeCouponFromDB( Coupon coupon ) {
		 //create inbox message in inbox DB Table
		Inbox inbox = new Inbox(InboxUtils.getNextID(), "Coupon " + coupon.getID() + ":" + coupon.getTitle() 
		+ " was expired and removed");
		Logger.inboxLog(inbox);
		
		int ownerID = -1; //should be positive in case we have any owner gotten from DB table
		try {// check if company has a coupon (most likely there will always be company owner)
			if ( (ownerID = _couponDao.getCouponOwnerID(coupon, SqlDB.COMPANY_COUPON_TABLE)) > -1 ) {
				// remove it from company_coupon join table
				_couponDao.removeCouponFromOwner(coupon.getID(), SqlDB.COMPANY_COUPON_TABLE);
				//Create entry for inbox mesage in company inbox Join table
				//in other words send notification to Owner inbox
				Logger.updateOwnerInbox(ownerID, inbox.getID(), SqlDB.COMPANY_INBOX_TABLE);
			}
			ownerID = -1; // do the same for customer - means if there is any customer purchased a coupon
			if ( (ownerID = _couponDao.getCouponOwnerID(coupon, SqlDB.CUSTOMER_COUPON_TABLE)) > -1 ) {
				_couponDao.removeCouponFromOwner(coupon.getID(), SqlDB.CUSTOMER_COUPON_TABLE);
				Logger.updateOwnerInbox(ownerID, inbox.getID(), SqlDB.CUSTOMER_INBOX_TABLE);
			}
			_couponDao.removeCoupon(coupon);//remove it from coupon DB table
			System.out.println("\t\tCoupon removed");
			String msg = "Coupon " + coupon.getID() + ":" + coupon.getTitle() + " expired and removed";
			Logger.log(msg, LogLevel.INFO);
			Logger.log(msg, LogType.COUPON, LogLevel.INFO);
		} catch (CouponSqlException e) {
			SqlExceptionUtils.couponException(e);
		}
	}
		
	// Force Stop this Task
	public void stopTask() {
		_quite = true;
		Logger.log("Daily Expiration Task was shutdown by demand", LogLevel.INFO);
	}
	
}