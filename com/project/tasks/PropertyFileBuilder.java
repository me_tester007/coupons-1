package com.project.tasks;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.project.CouponSystem;
import com.project.constants.FileNames;
import com.project.daofile.LogLevel;
import com.project.logs.Logger;

/**
 * Property builder class if system starts for the first time
 * and there is no property file - the default one is automatically generated
 * */
public class PropertyFileBuilder {

	private static final String[] _propertyStr = {	
			"\n",
			"#	THIS IS COUPON SYSTEM'S PROPERTY CONFIGURATION FILE\n",
			"#<<<====================================================>>>\n",
			"\n",
			"# USE VALUES WITH NO QUOTATIONS\n",
			"\n",
			"# System Admin Credentials\n",
			"adminUser=admin\n",
			"adminPassword=admin123\n",
			"\n",
			"# CURRENT IMPLEMENTATION WORKS WITH MYSQL AND XML ONLY\n",
			"# DATABASE DETAILS\n",
			"# dbType accepts either 'db' or 'xml' - (if xml chosen other sql parameteres are ignored)\n",
			"# dbVendor accepts - 'mysql'\n",
			"# dbIntallationType accepts either 'connect' or 'create' (schema)\n",
			"dbType=xml\n",
			"dbVendor=mysql\n",
			"driverName=com.mysql.jdbc.Driver\n",
			"dbHostname=localhost\n",
			"dbPort=3306\n",
			"dbUsername=root\n",
			"dbPassword=root@mysql\n",
			"dbSchemaName=coupons\n",
			"dbInstallationType=connect\n",
			"\n",
			"# DAO LAYERS DETAILS\n",
			"# DAO Full Class path name\n",
			"# com.project.daoxml - for xmldao\n",
			"# com.project.daosql - for sqldao (this option requires dbType=db)\n",
			"classname=com.project.daoxml\n",
			"\n",
			"# LOGS\n",
			"# LogLevel accepts - INFO,DEBUG\n",
			"logLevel=INFO\n",
			""
	};

	public static String[] getPropertyStr(){
		return _propertyStr;
	}

	public static void invokePropertyCreation() {
		invokePropertyCreation( _propertyStr );
	}

	public static void invokePropertyCreation( String[] propertyStr ) {
		
		checkIfPropertyExist();
		
		try (FileWriter writer = new FileWriter(FileNames.PROPERTY_FILE_NAME)) {
			
			for (String string : propertyStr)
				writer.write(string);
		} catch (IOException e) {
			Logger.log("Property File Error occured", LogLevel.ERROR);
			if ( CouponSystem.getInstance() != null )
				CouponSystem.getInstance().shutdown();
		}
	}
	
	private static boolean checkIfPropertyExist() {
		try {
			File propertyDir = new File(FileNames.PROPERTY_DIR);
			File propertyFile = new File(FileNames.PROPERTY_FILE_NAME);
			if ( ! propertyFile.exists() ) {
				if ( ! propertyDir.exists() )
					propertyDir.mkdir();
			}
			propertyFile.createNewFile();
		} catch (IOException e) {
			Logger.log("error creating property file", LogLevel.FATAL);
		}
		return true;
	}


}
