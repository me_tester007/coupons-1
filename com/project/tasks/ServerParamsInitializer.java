package com.project.tasks;

import java.io.File;
import java.util.Properties;

import com.project.constants.FileNames;
import com.project.constants.LogDetails;
import com.project.constants.ServerAdmin;
import com.project.constants.SqlDB;
import com.project.dao.DaoSchemaHandler;
import com.project.dao.DbVendorDao;
import com.project.daofile.LogLevel;
import com.project.daosql.SchemaHandlerDBDao;
import com.project.daoxml.XmlFileHandler;
import com.project.exceptions.sql.SqlRunTimeException;
import com.project.logs.Logger;
import com.project.utils.SqlExceptionUtils;

/**
 * Class is used to load on server startup and initialize DB SQL type and its parameters
 * */
public class ServerParamsInitializer {

	// TODO use this class to add functions responsible for 
	// server's parameters initialzing 
	
	/**
	 * Initialize SQL Related static initial parameters
	 * */
	public static void initSqlParameters( Properties properties ) {
		if ( ! properties.getProperty("dbType").equalsIgnoreCase("db") )
			return;
		
		if ( (SqlDB.DBVENDOR = properties.getProperty("dbVendor")) == null )
			SqlDB.DBVENDOR = DbVendorDao.UNDEFINED.toString();
		if ( (SqlDB.HOST = properties.getProperty("dbHostname")) == null )
			SqlDB.HOST = "UNDEFINED";
		if ( (SqlDB.USERNAME = properties.getProperty("dbUsername")) == null )
			SqlDB.USERNAME = "UNDEFINED";
		if ( (SqlDB.PASSWORD = properties.getProperty("dbPassword")) == null )
			SqlDB.PASSWORD = "UNDEFINED";
		if ( (SqlDB.DRV_NAME = properties.getProperty("driverName")) == null )
			SqlDB.DRV_NAME = "UNDEFINED";
		if ( (SqlDB.DB_INSTALL_TYPE = properties.getProperty("dbInstallationType")) == null )
			SqlDB.DB_INSTALL_TYPE = "UNDEFINED";
		if ( (SqlDB.NAME = properties.getProperty("dbSchemaName")) == null )
			SqlDB.NAME = "coupons";
		
		try {
			SqlDB.PORT = Integer.parseInt(properties.getProperty("dbPort"));
		} catch (NumberFormatException e) {
			SqlDB.PORT = 5432;
		}
		// when all parameters loaded (above) construct DB URL
		SqlDB.buildFullUrl();
		
		// When all required procedures initialized - start creating initial Schema and tables 
		// in case dbInstallationType set to "create" option in properties file
		_createInitialSchema(properties.getProperty("dbInstallationType"),
				properties.getProperty("dbSchemaName"));
		
	}
	
	public static void initXmlParameters( Properties properties ) {
		if ( properties == null || ! properties.getProperty("dbType").equalsIgnoreCase("xml") )
			return;
		
		if ( ! XmlFileHandler.xmlAllDbFilesExist() )
			XmlFileHandler.createXmlDBFiles();
	}
	
	/**
	 * Initialize Log related static initial parameter
	 * */
	public static void initLogParameters( Properties properties ) {
		
		if ( (LogDetails.currentLogLevel = LogLevel.valueOf(properties.getProperty("logLevel"))) == null )
			LogDetails.currentLogLevel = LogLevel.INFO;
		
		if ( ! createLogDir() ) {
			Logger.log("error creating log directory", LogLevel.ERROR);
			System.exit(1);
		}
			
	}
	
	public static void initServerAdminCredentials( Properties properties ) {
		
		if ( (ServerAdmin.USERNAME = properties.getProperty("adminUser")) == null )
			ServerAdmin.USERNAME = "admin";
		if ( (ServerAdmin.PASSWORD = properties.getProperty("adminPassword")) == null )
			ServerAdmin.PASSWORD = "1234";	
	}
	
	// Creates inital Schema and Tables in case such configured in properties file
	private static void _createInitialSchema( String dbInstallType, String schemaName ) {
		if ( dbInstallType == null ) {
			Logger.log("dbInstallation Type not SET. Using default \"connect\" mode ", LogLevel.ERROR);
			return;
		}
		
		//Schema is assumed to be existing if user configured 'connect' in system property file
		if ( dbInstallType.equalsIgnoreCase("connect") ){
			Logger.log("dbInstallation set to connect mode", LogLevel.DEBUG);
			return;
		}
		
		/* 
		 * If we reached this line the only correct value should be 'create'
		 * and if we do not get that value the customer misconfigured the system property file 
		*/
		if ( ! dbInstallType.equalsIgnoreCase("create") ) {
			Logger.log("Illegal value for dbInstallaType recieved: " + dbInstallType + "\nSHUTING DOWN ", LogLevel.FATAL);
			System.exit(1);
		}
		System.out.println("Creating DB Sceham: " + schemaName);
		Logger.log("Creating DB Sceham: " + schemaName, LogLevel.DEBUG);
		DaoSchemaHandler schemah = new SchemaHandlerDBDao();
		try {
			if ( ! schemah.createSchema(schemaName) ) {
				Logger.log("DB Default Schema creation failed\nSHUTING DOWN", LogLevel.ERROR);
				System.exit(1);
			}
			System.out.println("DB schema created successfully\nPopulating schema with required tables");
			schemah.initializeDefaultTables(SqlDB.NAME);
			Logger.log("DB Default Schema " + schemaName +" created successfully", LogLevel.DEBUG);
			Logger.log("DB Default Schema Tables created successfully", LogLevel.DEBUG);
		} catch ( SqlRunTimeException e ) {
			SqlExceptionUtils.connectionFail(e);
			System.exit(1);
		}
		System.out.println("All required tables successfully created");
		Logger.log("All initial DB opretaions successfully performed: " + schemaName, LogLevel.INFO);
	}

	private static boolean createLogDir() {

		File propertyDir = new File(FileNames.LOGS_DIR);
		if ( ! propertyDir.exists() )
			if ( ! propertyDir.mkdir() )
				return false;
		return true;
	}


}
