package com.project.daoxml;

import java.util.Collection;
import java.util.HashSet;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.project.beans.Accessory;
import com.project.beans.BeanClassType;
import com.project.beans.Inbox;
import com.project.constants.XmlDB;
import com.project.dao.InboxDao;

/**
* DAO Layer Class working with XML DB server
* */
public class InboxDBDao implements InboxDao {

	//Constructor
	public InboxDBDao() {}

	// creates new inbox message in the xml file
	@Override
	public void createInboxMessage( Inbox inbox ) {
		Document doc = null;
		if (DaoXmlHandler.isXMLEmpty(XmlDB.INBOX_XMLFILE))
			doc = DaoXmlHandler.newDoc(XmlDB.INBOXES_ELEMENT);
		else {
			doc = DaoXmlHandler.readFromXMLFile(XmlDB.INBOX_XMLFILE);
		}
		String id = Integer.toString(inbox.getID());
		Element inboxesRootNode = doc.getDocumentElement();
		Element newInbox = doc.createElement("inbox");
		newInbox.setAttribute("id", id);
		inboxesRootNode.appendChild(newInbox);
		DaoXmlHandler.createAppendAndInsertToXML(doc,newInbox,"id", id);
		DaoXmlHandler.createAppendAndInsertToXML(doc,newInbox,"message", inbox.getMessage());
		DaoXmlHandler.writeToXMLFile(doc,XmlDB.INBOX_XMLFILE);
	}

	// removes inbox message by specific inbox given.
	@Override
	public void removeInboxMessage( Inbox inbox ) {
		
		DaoXmlHandler.removeAccessory(inbox, XmlDB.INBOX_XMLFILE, BeanClassType.INBOX);
	}

	// updates an inbox by removing it and re create 
	@Override
	public void updateInboxMessage( Inbox inbox ) {
		removeInboxMessage(inbox);
		createInboxMessage(inbox);
	}

	// gets all messages 
	@Override
	public Collection<Inbox> getAllMessages() {
		
		Collection<Accessory> accessories = DaoXmlHandler.getAllAccessories(XmlDB.INBOX_XMLFILE, BeanClassType.INBOX);
		Collection<Inbox> inboxes = new HashSet<>();
		
		for (Accessory accessory : accessories) {
			inboxes.add((Inbox)accessory);
		}
		return inboxes;
	}

	// returns an inbox by customer id
	@Override
	public Inbox getInboxMessage( int id ) {
		
		return (Inbox)DaoXmlHandler.getAccessory(id, XmlDB.INBOX_XMLFILE, BeanClassType.INBOX);	
	}

	@Override
	public void assignInboxToOwner( int ownerID, int inboxID, String filePath ) {
		if ( filePath.contains("customer") )
			filePath = XmlDB.CUSTOMER_XMLFILE;
		else filePath = XmlDB.COMPANY_XMLFILE;
		DaoXmlHandler.assignAccessoryToOwner(ownerID, inboxID, filePath, XmlDB.INBOX_ELEMENT);
	}

	@Override
	public void removeInboxFromOwner( int inboxID, String filePath ) {
		if ( filePath.contains("customer") )
			filePath = XmlDB.CUSTOMER_XMLFILE;
		else filePath = XmlDB.COMPANY_XMLFILE;
		Document doc = DaoXmlHandler.readFromXMLFile(filePath);
		Element ownerElement = null;
		Node ownerNode = null;
		XPath xPath =  XPathFactory.newInstance().newXPath();
		String expression =	"//*[@id='"+inboxID+"']";;
		try {
			ownerNode = (Node) xPath.compile(expression).evaluate(doc, XPathConstants.NODE);
			if (ownerNode == null){
				System.out.println("wrong insert"); //TODO : throw exception
				return;
			}
			if (ownerNode.getNodeType() == Node.ELEMENT_NODE)
				ownerElement = (Element) ownerNode;
			if ( ownerElement.getAttribute("id").equals(Integer.toString(inboxID))){
				ownerElement.getParentNode().removeChild(ownerNode);
				DaoXmlHandler.writeToXMLFile(doc, filePath);
			}
		}catch (XPathExpressionException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void removeInboxOwner( int ownerID, String ownerName, String filePath ) {
		if ( filePath.contains("customer") )
			filePath = XmlDB.CUSTOMER_XMLFILE;
		else filePath = XmlDB.COMPANY_XMLFILE;
		DaoXmlHandler.dissociateOwner(ownerID, ownerName, filePath);
	}

	@Override
	public int getInboxLatestID() {
		
		return DaoXmlHandler.getLatestID(XmlDB.INBOX_XMLFILE);
	}

}
