package com.project.daoxml;

import java.util.Collection;
import java.util.HashSet;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.project.beans.Accessory;
import com.project.beans.BeanClassType;
import com.project.beans.Coupon;
import com.project.beans.CouponType;
import com.project.constants.XmlDB;
import com.project.dao.CouponDao;

/**
* DAO Layer Class working with XML DB server
* */
public class CouponDBDao implements CouponDao{

	// Constructor
	public CouponDBDao() {}

	// Creates a new coupon in the XMl file & checks if theres 
	// a coupon with the same id to avoid duplications
	@Override
	public void createCoupon( Coupon coupon ) {
		Document doc = null;
		if (DaoXmlHandler.isXMLEmpty(XmlDB.COUPON_XMLFILE))
			doc = DaoXmlHandler.newDoc(XmlDB.COUPONS_ELEMENT);
		else {
			doc = DaoXmlHandler.readFromXMLFile(XmlDB.COUPON_XMLFILE);
		}
		Coupon check = getCoupon(coupon.getID());
		if (check != null){
			System.out.println("Coupon already exsists");
			return;
			// TODO : throw exception
		}
		String id = Integer.toString(coupon.getID()); 
		String amount = Integer.toString(coupon.getAmount());
		String price = Double.toString(coupon.getPrice());
		Element couponsRootNode = doc.getDocumentElement();
		Element newCoupon = doc.createElement("coupon");
		newCoupon.setAttribute("id", id);
		couponsRootNode.appendChild(newCoupon);
		DaoXmlHandler.createAppendAndInsertToXML(doc,newCoupon,"title", coupon.getTitle());
		DaoXmlHandler.createAppendAndInsertToXML(doc,newCoupon,"id", id);
		DaoXmlHandler.createAppendAndInsertToXML(doc,newCoupon,"startDate",coupon.getStartDate().toString());
		DaoXmlHandler.createAppendAndInsertToXML(doc,newCoupon,"endDate", coupon.getEndDate().toString());
		DaoXmlHandler.createAppendAndInsertToXML(doc,newCoupon,"amount", amount);	
		DaoXmlHandler.createAppendAndInsertToXML(doc,newCoupon,"message", coupon.getMessage());
		DaoXmlHandler.createAppendAndInsertToXML(doc,newCoupon,"price", price);
		DaoXmlHandler.createAppendAndInsertToXML(doc,newCoupon,"image", coupon.getImage());
		DaoXmlHandler.createAppendAndInsertToXML(doc,newCoupon,"type", coupon.getType().toString());
		DaoXmlHandler.writeToXMLFile(doc,XmlDB.COUPON_XMLFILE);
	}

	// removes the given coupon and update the XMl file.
	@Override
	public void removeCoupon( Coupon coupon ) {
		
		DaoXmlHandler.removeAccessory(coupon, XmlDB.COUPON_XMLFILE, BeanClassType.COUPON);
	}

	// updates the given coupon by removing it and create it again in the file
	@Override
	public void updateCoupon( Coupon coupon ) {
		removeCoupon(coupon);
		createCoupon(coupon);
	}

	// returns a coupon object from the xml file by id
	@Override
	public Coupon getCoupon( int id ) {
		
		return (Coupon)DaoXmlHandler.getAccessory(id, XmlDB.COUPON_XMLFILE, BeanClassType.COUPON);	
	}

	// returns all of the coupons in a coupon collection from the xml file 
	@Override
	public Collection<Coupon> getAllCoupons() {
		
		Collection<Accessory> accessories = DaoXmlHandler.getAllAccessories(XmlDB.COUPON_XMLFILE, BeanClassType.COUPON);
		if ( accessories == null )
			return null;
		Collection<Coupon> coupons = new HashSet<>();
		for (Accessory accessory : accessories) {
			coupons.add((Coupon)accessory);
		}
		return coupons;
	}

	// return all coupons by specific type
	@Override
	public Collection<Coupon> getCouponByType( CouponType couponType ) {
		Document doc = DaoXmlHandler.readFromXMLFile(XmlDB.COUPON_XMLFILE);
		if (DaoXmlHandler.isXMLEmpty(XmlDB.COUPON_XMLFILE))
			return null;
		Element coupons = doc.getDocumentElement();
		NodeList couponsList = coupons.getChildNodes();
		Coupon newCoupon = new Coupon();
		String couponTypeStr = couponType.toString();
		Collection<Coupon> collection = new HashSet<>();
		for (int i = 0; i < couponsList.getLength(); i++) {
			// Node that represents a current coupon in the coupon list
			Node coupon = couponsList.item(i);	
			if (coupon.getNodeType() == Node.ELEMENT_NODE && coupon.getLastChild().getTextContent().equals(couponTypeStr)){
				newCoupon = getCoupon(Integer.parseInt(coupon.getAttributes().item(0).getTextContent()));
				collection.add(newCoupon);
			}
		}
		return collection;
	}

	// gets a coupon id and owner id - could be company or customer and assaign to coupon to her xml tag
	@Override
	public void assignCouponToOwner( int ownerID, int couponID, String filePath ) {
		if ( filePath.contains("customer") )
			filePath = XmlDB.CUSTOMER_XMLFILE;
		else filePath = XmlDB.COMPANY_XMLFILE;
		DaoXmlHandler.assignAccessoryToOwner(ownerID, couponID, filePath, XmlDB.COUPONS_ELEMENT);
	}

	//	remove the coupon from the owner in the xml file
	@Override
	public void removeCouponFromOwner( int couponID, String filePath ) {
		if ( filePath.contains("customer") )
			filePath = XmlDB.CUSTOMER_XMLFILE;
		else filePath = XmlDB.COMPANY_XMLFILE;
		Node couponNode = null;
		Document doc = DaoXmlHandler.readFromXMLFile(filePath);
		XPath xPath =  XPathFactory.newInstance().newXPath();
		String expression =	"//*[@id='"+couponID+"']";;
		try {
			couponNode = (Node) xPath.compile(expression).evaluate(doc, XPathConstants.NODE);
			if (couponNode == null) {
				System.out.println("wrong insert"); //TODO : throw exception
				return;
			}
			couponNode.getParentNode().removeChild(couponNode);
			DaoXmlHandler.writeToXMLFile(doc,filePath);
		}
		catch (XPathExpressionException e) {
			e.printStackTrace();
		}
	}
	// search for owner ID , verify his name and remove node from the file
	@Override
	public void removeCouponOwner( int ownerID, String ownerName, String filePath ) {
		if ( filePath.contains("customer") )
			filePath = XmlDB.CUSTOMER_XMLFILE;
		else filePath = XmlDB.COMPANY_XMLFILE;
		DaoXmlHandler.dissociateOwner(ownerID, ownerName, filePath);
	}

	@Override
	public int getCouponOwnerID( Coupon coupon, String filePath ) {
		if ( filePath.contains("customer") )
			filePath = XmlDB.CUSTOMER_XMLFILE;
		else filePath = XmlDB.COMPANY_XMLFILE;
		int id = 0;
		Document doc = DaoXmlHandler.readFromXMLFile(filePath);
		Element ownerElement = null;
		Node ownerNode = null;
		XPath xPath =  XPathFactory.newInstance().newXPath();
		String expression =	"//*[@id='"+coupon.getID()+"']";;
		try {
			ownerNode = (Node) xPath.compile(expression).evaluate(doc, XPathConstants.NODE);
			if (ownerNode == null){
				System.out.println("wrong insert"); //TODO : throw exception
				return id;
			}
			if (ownerNode.getNodeType() == Node.ELEMENT_NODE){
				ownerElement = (Element) ownerNode;
				id = Integer.parseInt((ownerElement.getParentNode().getParentNode().getAttributes().getNamedItem("id").getTextContent()));
			}
		}catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return id;
	}

	@Override
	public int getCouponLatestID() {
		
		return DaoXmlHandler.getLatestID(XmlDB.COUPON_XMLFILE);
	}

}
