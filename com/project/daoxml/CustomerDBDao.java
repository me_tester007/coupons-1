package com.project.daoxml;

import java.util.Collection;
import java.util.HashSet;

import com.project.beans.Accessory;
import com.project.beans.BeanClassType;
import com.project.beans.Coupon;
import com.project.beans.Customer;
import com.project.beans.Inbox;
import com.project.beans.Owner;
import com.project.constants.XmlDB;
import com.project.dao.CustomerDao;

/**
* DAO Layer Class working with XML DB server
* */
public class CustomerDBDao implements CustomerDao {

	// constructor 
	public CustomerDBDao() {}

	// Creates a new customer in the XMl file & checks if theres 
	// a customer with the same id to avoid duplications
	@Override
	public void createCustomer( Customer customer ) {
		
		DaoXmlHandler.createOwner(customer, XmlDB.CUSTOMERS_ELEMENT, XmlDB.CUSTOMER_XMLFILE, BeanClassType.CUSTOMER);
	}

	// removes the given customer and update the XMl file.
	@Override
	public void removeCustomer( Customer customer ) {
		
		DaoXmlHandler.removeOwner(customer, XmlDB.CUSTOMER_XMLFILE, BeanClassType.CUSTOMER);
	}

	// updates the given customer  by removing it and create it again in the file
	@Override
	public void updateCustomer( Customer c ) {
		removeCustomer(c);
		createCustomer(c);
	}

	// returns a customer object from the xml file by id
	@Override
	public Customer getCustomer( int id ) {
		
		return (Customer)DaoXmlHandler.getOwner(id, XmlDB.CUSTOMER_XMLFILE, BeanClassType.CUSTOMER);	
	}
	
	// returns a customer object from the xml file by name
	@Override
	public Customer getCustomer( String name ) {
		
		return (Customer)DaoXmlHandler.getOwner(name, XmlDB.CUSTOMER_XMLFILE, BeanClassType.CUSTOMER);	
	}

	// returns all of the customers in a customer collection from the xml file 
	@Override
	public Collection<Customer> getAllCustomers() {
		
		Collection<Owner> owners = DaoXmlHandler.getAllOwners(XmlDB.CUSTOMER_XMLFILE, BeanClassType.CUSTOMER);
		Collection<Customer> customers = new HashSet<>();
		
		for (Owner owner : owners)
			customers.add((Customer)owner);

		return customers;
	}

	// returns all of the customer coupons by given id in a coupon collection from the xml file
	@Override
	public Collection<Coupon> getCoupons( int customerID ) {
		
		Collection<Accessory> accessories = DaoXmlHandler.getCoupons(customerID, XmlDB.CUSTOMER_XMLFILE, XmlDB.COUPONS_ELEMENT, BeanClassType.COUPON);
		Collection<Coupon> coupons = new HashSet<>();
		
		for (Accessory accessory : accessories)
			coupons.add((Coupon)accessory);

		return coupons;
	}

	// gets all of the customers messages in a inbox collection
	@Override
	public Collection<Inbox> getInboxMessages( int customerID ) {

		Collection<Accessory> accessories = DaoXmlHandler.getInboxMessages(customerID, XmlDB.CUSTOMER_XMLFILE, XmlDB.INBOX_ELEMENT);
		Collection<Inbox> inboxes = new HashSet<>();
		
		for (Accessory accessory : accessories)
			inboxes.add((Inbox)accessory);
		
		return inboxes;
	}

	
	@Override
	public boolean login(String customerName, String password) {
		
		return DaoXmlHandler.login(XmlDB.CUSTOMER_XMLFILE, customerName, password);
	}

	@Override
	public int getCustomerLatestID() {
		return DaoXmlHandler.getLatestID(XmlDB.CUSTOMER_XMLFILE);
	}

}
