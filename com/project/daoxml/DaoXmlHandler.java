package com.project.daoxml;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import com.project.beans.Accessory;
import com.project.beans.BeanClassType;
import com.project.beans.Coupon;
import com.project.beans.CouponType;
import com.project.beans.Inbox;
import com.project.beans.Owner;
import com.project.constants.XmlDB;
import com.project.exceptions.DuplicateOwnerException;
import com.project.exceptions.GenRunTimeException;
import com.project.exceptions.xml.XmlRunTimeException;

/**
* DAO Single Class for manipulating all xmldb related operations
* used by all XMLDAO classes
* */
public class DaoXmlHandler {

	private DaoXmlHandler() {}

	// reads an xml file, parse it and stores it in a Document obj
	public static Document readFromXMLFile( String xmlFilename) throws XmlRunTimeException {

		//File xmlFile = new File(xmlFilename);
		XmlFileConnection conn = XmlFileConnection.dispatcherInstance(xmlFilename);
		if ( conn == null )
			throw new XmlRunTimeException("connection is null");
		File xmlFile = conn.getConnection();

		DocumentBuilderFactory dbf;
		DocumentBuilder db;
		Document doc = null;
		dbf = DocumentBuilderFactory.newInstance();
		try {
			db = dbf.newDocumentBuilder();
			doc = db.parse(xmlFile);
			return doc;
		} catch (SAXException e) {
			throw new XmlRunTimeException(e, XmlRunTimeException.SAX_ERR );
		} catch (IOException e) {
			throw new XmlRunTimeException(e, XmlRunTimeException.IO_ERR);
		} catch (ParserConfigurationException e) {
			throw new XmlRunTimeException(e, XmlRunTimeException.PARSE_ERR);
		} finally {
			conn.returnConnection(xmlFile);
		}
	}

	// creates a new document and returns it for future usage
	public static Document newDoc( String element ) throws XmlRunTimeException {
		Document doc = null;
		DocumentBuilderFactory dbf;
		DocumentBuilder db;
		dbf = DocumentBuilderFactory.newInstance();
		try {
			db = dbf.newDocumentBuilder();
			doc = db.newDocument();
			Element rootElement = doc.createElement(element);
			doc.appendChild(rootElement);
			return doc;
		} catch (ParserConfigurationException e) {
			throw new XmlRunTimeException(e, XmlRunTimeException.PARSE_CONF_ERR);
		}
	}

	// gets a document and write it to the xml file that was defined
	public static void writeToXMLFile( Document doc, String xmlFilename ) throws XmlRunTimeException {
		//File xmlFile = new File(xmlFilename);
		//System.out.println("wTOxml: filename " + xmlFilename);
		XmlFileConnection conn = XmlFileConnection.dispatcherInstance(xmlFilename);
		File xmlFile = conn.getConnection();
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
		try {
			transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(xmlFile);
			transformer.transform(source, result);
		} catch (TransformerException e) {
			throw new XmlRunTimeException(e, XmlRunTimeException.TRANSFORMER);
		} finally {
			conn.returnConnection(xmlFile);
		}
		//System.out.println("Changes saved");
	}

	// checks if the defined xml file is empty
	public static boolean isXMLEmpty( String xmlFilename ) {
		//File xmlFile = new File(xmlFilename);
		Boolean empty = true;
		XmlFileConnection conn = XmlFileConnection.dispatcherInstance(xmlFilename);
		if ( conn == null )
			return true;
		File xmlFile = conn.getConnection();

		if (xmlFile.exists() && xmlFile.length() != 0)
			empty = false;

		conn.returnConnection(xmlFile);
		return empty;
	}

	// creates an XML element in a dom and append it for future usage in creating elements
	public static void createAppendAndInsertToXML( Document doc, Element dest, String newElementName, String textNode ) {
		Element element = doc.createElement(newElementName);
		dest.appendChild(element);
		Text text = doc.createTextNode(textNode);
		element.appendChild(text);
	}

	// build new company bean by element for the getCompany methods.
	private static Owner _buildNewOwnerByElement( String name, String filename, String classname ) {

		Owner newOwner = null;
		try {
			newOwner = (Owner) Class.forName(classname).newInstance();
		} catch (InstantiationException e) {
			throw new GenRunTimeException(e.getMessage(), GenRunTimeException.INSTANTIATION);
		} catch (IllegalAccessException e) {
			throw new GenRunTimeException(e.getMessage(), GenRunTimeException.ILLEGALACCESS);
		} catch (ClassNotFoundException e) {
			throw new GenRunTimeException(e.getMessage(), GenRunTimeException.CLASS_FOUND);
		}

		Document doc = readFromXMLFile(filename);
		Element owner = doc.getDocumentElement();
		NodeList list = owner.getElementsByTagName(name);
		NodeList currentOwner = list.item(0).getChildNodes();
		Node[] atts = new Node[6];
		for (int i = 0; i < currentOwner.getLength(); i++) {
			atts[i] = (Node) currentOwner.item(i);
		}
		int id = Integer.parseInt(atts[1].getTextContent());
		newOwner.setName(name);
		newOwner.setID(id);
		newOwner.setPassword(atts[2].getTextContent());
		newOwner.setEmail(atts[3].getTextContent());
		return newOwner;
	}

	/// returns a company object from the xml file by id
	public static Owner getOwner( int id, String filename, String classname ) {
		Owner newOwner = null;
		if (isXMLEmpty(filename))
			return null;
		Document doc = readFromXMLFile(filename);
		XPath xPath =  XPathFactory.newInstance().newXPath();
		String expression =	"//*[@id='"+id+"']";;
		try {
			NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
			if (nodeList.getLength() == 0)
				return null;
			newOwner = _buildNewOwnerByElement(nodeList.item(0).getNodeName(), filename, classname);		
		} catch (XPathExpressionException e) {
			throw new XmlRunTimeException(e, XmlRunTimeException.XPATH_ERROR);
		}
		return newOwner; 	
	}

	/// returns a company object from the xml file by name
	public static Owner getOwner( String name, String filename, String classname ) {
		Owner newOwner = null;
		if (isXMLEmpty(filename))
			return null;
		Document doc = readFromXMLFile(filename);
		XPath xPath =  XPathFactory.newInstance().newXPath();
		String expression = "//" + name;
		try {
			NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
			if (nodeList.getLength() == 0)
				return null;
			newOwner = _buildNewOwnerByElement(name, filename, classname);		
		} catch (XPathExpressionException e) {
			throw new XmlRunTimeException(e, XmlRunTimeException.XPATH_ERROR);
		}
		return newOwner; 	
	}

	// Creates a new company in the XMl file & checks if theres 
	// a company with the same id to avoid duplications
	public static void createOwner( Owner owner, String element, String filename, String classname ) {
		Document doc = null;
		if (isXMLEmpty(filename))
			doc = newDoc(element);
		else {
			doc = readFromXMLFile(filename);
		}
		Owner check = getOwner(owner.getID(), filename, classname);
		if (check != null){
			System.out.println("Company already exsists");
			throw new DuplicateOwnerException("Owner already exsists " + owner.getEmail());
		}
		String id = Integer.toString(owner.getID());
		Element ownersRootNode = doc.getDocumentElement();
		Element newOwner = doc.createElement(owner.getName());
		newOwner.setAttribute("id", id);
		ownersRootNode.appendChild(newOwner);
		createAppendAndInsertToXML(doc,newOwner,"name", owner.getName());
		createAppendAndInsertToXML(doc,newOwner,"id", id);
		createAppendAndInsertToXML(doc,newOwner,"password",owner.getPassword());
		createAppendAndInsertToXML(doc,newOwner,"email", owner.getEmail());
		createAppendAndInsertToXML(doc,newOwner,"coupons"," ");//c.getCoupons()
		createAppendAndInsertToXML(doc,newOwner,"inbox", " ");//c.getInboxMessage()
		writeToXMLFile(doc, filename);
	}

	// removes the given company and update the XMl file.
	public static void removeOwner( Owner owner, String filename, String classname ) {

		if (isXMLEmpty(filename))
			return;
		Document doc = readFromXMLFile(filename); 
		if (getOwner(owner.getName(), filename, classname) == null) {
			System.out.println("Owner doesnt exists " + owner.getEmail());
			return;
		}
		Element root = doc.getDocumentElement();
		Element newOwner = (Element) root.getElementsByTagName(owner.getName()).item(0);
		newOwner.getParentNode().removeChild(newOwner);
		doc.normalize();
		writeToXMLFile(doc, filename);
	}

	// returns all of the owners in a owner collection from the xml file
	public static Collection<Owner> getAllOwners( String filename, String classname ) {

		if (isXMLEmpty(filename))
			return null;

		Owner newOwner = null;
		Document doc = readFromXMLFile(filename);
		Element owners = doc.getDocumentElement();
		NodeList list = owners.getChildNodes();
		Collection<Owner> ownerCollection = new HashSet<>();
		for (int i = 0; i < list.getLength(); i++) {
			Node owner = list.item(i);
			newOwner = getOwner(owner.getNodeName(), filename, classname);
			ownerCollection.add(newOwner);
		}
		return ownerCollection;
	}

	// returns all of the owners coupons by id in a coupon collection from the xml file	
	public static Collection<Accessory> getCoupons( int ownerID, String filename, String el, String accessoryType ) {	
		if (isXMLEmpty(filename))
			return null;
		//CouponDao couponDao = new CouponDBDao(); // TODO change to use DBDAO SINGLETON
		Accessory newAccessory = null;
		Document doc = readFromXMLFile(filename);
		Element currentOwnerElement = null;
		//NodeList coupons = null;
		Collection <Accessory> collection = new HashSet<>();
		//Element companyElement = null;
		Element ownerAccessoryElement = null;
		int accessoryID = 0;
		//Element root = doc.getDocumentElement();
		//NodeList companies = root.getChildNodes();
		XPath xPath =  XPathFactory.newInstance().newXPath();
		String expression =	"//*[@id='" + ownerID + "']";
		try {
			Node owner = (Node) xPath.compile(expression).evaluate(doc, XPathConstants.NODE);
			NodeList currentOwnerNodesList = owner.getChildNodes();
			for (int i = 0; i < currentOwnerNodesList.getLength(); i++) {
				if (currentOwnerNodesList.item(i).getNodeType() == Node.ELEMENT_NODE){
					currentOwnerElement = (Element) currentOwnerNodesList.item(i);
					if (currentOwnerElement.getTagName().equals(el)){
						ownerAccessoryElement = currentOwnerElement;
					}
				}
			}
			Element element = null;
			NodeList accessoryNodes = ownerAccessoryElement.getChildNodes();
			for (int i = 0; i < accessoryNodes.getLength(); i++){
				if (accessoryNodes.item(i).getNodeType() == Node.ELEMENT_NODE && accessoryNodes.item(i).hasAttributes()){
					element = (Element) accessoryNodes.item(i);
					accessoryID =Integer.parseInt(element.getAttributeNode("id").getValue());
					//newAccessory = couponDao.getCoupon(couponID);
					newAccessory = getAccessory(accessoryID, filename, accessoryType);
					collection.add(newAccessory);
				} 
			}
		} catch (XPathExpressionException e) {
			throw new XmlRunTimeException(e, XmlRunTimeException.XPATH_ERROR);
		}
		return collection;
	}

	// returns all of the owmers messages by id in a inbox collection from the xml file
	public static Collection<Accessory> getInboxMessages( int ownerID, String filename, String element ) {
		// Use getCoupons function to retrive the Inbox collection
		//since the implementation of getCoupons and getInboxMessages are identical
		return getCoupons(ownerID, filename, element, BeanClassType.INBOX);
	}


	public static int getLatestID( String filename ) {
		if (isXMLEmpty(filename))
			return -1;
		Document doc = readFromXMLFile(filename);
		int id = 0;
		int maxId = 0;
		Node root = doc.getDocumentElement();
		NodeList objects = root.getChildNodes();
		for (int i = 0; i < objects.getLength(); i++) {
			id = Integer.parseInt(objects.item(i).getAttributes().getNamedItem("id").getTextContent());
			if (maxId < id){
				maxId = id;
			}
		}
		return maxId;
	}

	public static boolean login( String filename, String name, String pwd ) {
		if (isXMLEmpty(filename))
			return false;
		Document doc = readFromXMLFile(filename);	
		//Element root = doc.getElementById("name");
		NodeList names = doc.getElementsByTagName(name);
		if (names.getLength() != 1) {
			//		TODO : throw new exception
			return false;
		}
		Element logName = null;
		for (int i = 0; i < names.getLength(); i++) {
			if (names.item(i).getNodeType() == Node.ELEMENT_NODE){
				Element currentName = (Element) names.item(i);
				if (currentName.getTagName().equalsIgnoreCase(name)){
					logName = currentName;
					break;
				}
			}
		}
		if ( logName == null )
			return false;
		NodeList logNameChilds = logName.getElementsByTagName("password");
		return (logNameChilds.item(0).getTextContent().equals(pwd));
	}


	// search for owner ID , verify his name and remove node from the file
	public static void dissociateOwner( int ownerID, String ownerName, String filename ) {	
		Document doc = readFromXMLFile(filename);
		// TODO :if (isXMLEmpty(filename)) throw new exception
		Element ownerElement = null;
		Node ownerNode = null;
		XPath xPath =  XPathFactory.newInstance().newXPath();
		String expression =	"//*[@id='" + ownerID + "']";;
		try {
			ownerNode = (Node) xPath.compile(expression).evaluate(doc, XPathConstants.NODE);
			if (ownerNode == null){
				System.out.println("wrong insert"); //TODO : throw exception
				return;
			}
			if (ownerNode.getNodeType() == Node.ELEMENT_NODE)
				ownerElement = (Element) ownerNode;
			if (ownerElement.getTagName().equals(ownerName)&& ownerElement.getAttribute("id").equals(Integer.toString(ownerID))){
				ownerElement.getParentNode().removeChild(ownerNode);
				writeToXMLFile(doc, filename);
			}
		}catch (XPathExpressionException e) {
			throw new XmlRunTimeException(e, XmlRunTimeException.XPATH_ERROR);
		}
	}

	// gets a coupon id and owner id - could be company or customer and assaign to coupon to her xml tag
	public static void assignAccessoryToOwner( int ownerID, int accessoryID, String filePath, String element ) {
		//NodeList accessoryValidation = null;
		NodeList ownerNodes = null;
		Node owner = null;
		Element accessory = null;
		Document doc = readFromXMLFile(filePath);
		if ( doc == null )
			throw new XmlRunTimeException("Document is null");
		String innerElement = null;
		if ( element.equals(XmlDB.COUPONS_ELEMENT) )
			innerElement = XmlDB.COUPON_ELEMENT;
		else innerElement = XmlDB.INBOX_ELEMENT;
		XPath xPath =  XPathFactory.newInstance().newXPath();
		String ownerExpression ="//*[@id='" + ownerID + "']";;
		//String accessoryExpression = "//*[@id='" + accessoryID + "']";;
		try {
			owner = (Node) xPath.compile(ownerExpression).evaluate(doc, XPathConstants.NODE);
//			accessoryValidation = (NodeList) xPath.compile(accessoryExpression).evaluate(doc, XPathConstants.NODESET);
//			if ( accessoryValidation.getLength() == 1){
//				System.out.println("wrong insert");
//				return;
//			}	
			if (owner == null){
				System.out.println("owner does not exsists");
				return;
			}
			ownerNodes = owner.getChildNodes();
			for (int i = 0; i < ownerNodes.getLength(); i++) {
				if (ownerNodes.item(i).getNodeType() == Node.ELEMENT_NODE){
					accessory = (Element)ownerNodes.item(i);
					if (accessory.getTagName().equals(element)){
						Element accsry = doc.createElement(innerElement);
						accsry.setAttribute("id", Integer.toString(accessoryID));
						((Node) ownerNodes).getChildNodes().item(i).appendChild(accsry);
						writeToXMLFile(doc,filePath);
					}
				}
			}
		}catch (XPathExpressionException e) {
			throw new XmlRunTimeException(e, XmlRunTimeException.XPATH_ERROR);
		}
	}

	// returns a coupon/inbox object from the xml file by id
	public static Accessory getAccessory( int id, String filename, String accessoryType ) {
		if (isXMLEmpty(filename))
			return null;
		Accessory newAccessory = null;
		Document doc = readFromXMLFile(filename);
		XPath xPath =  XPathFactory.newInstance().newXPath();
		String idString = Integer.toString(id);
		String expression =	"//*[@id='" + id + "']";;
		try {
			NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
			if (nodeList.getLength() == 0)
				return null;
			newAccessory = ( accessoryType.equals(BeanClassType.INBOX)?
					buildNewInboxByElement(idString) :
						buildNewCouponByElement(idString) );		
		} catch (XPathExpressionException e) {
			throw new XmlRunTimeException(e, XmlRunTimeException.XPATH_ERROR);
		}
		return newAccessory; 	
	}

	//	build new inbox bean by element for the getInbox method.
	private static Inbox buildNewInboxByElement(String id){
		Inbox newInbox = new Inbox();
		Document doc = readFromXMLFile(XmlDB.INBOX_XMLFILE);
		NodeList inboxes = doc.getDocumentElement().getChildNodes();
		Node currentInbox = null;
		Element temp = null;
		for (int i = 0; i < inboxes.getLength(); i++) {
			if (inboxes.item(i).getNodeType() == Node.ELEMENT_NODE){
				temp = (Element)inboxes.item(i);
				if (temp.getAttribute("id").equals(id)){
					currentInbox = inboxes.item(i);
					break;
				}
			}
		}
		Node[] atts = new Node[2];
		NodeList attList = currentInbox.getChildNodes();
		for (int i = 0; i < attList.getLength(); i++) {
			atts[i] = attList.item(i);
		}
		int idInt = Integer.parseInt(atts[0].getTextContent());
		newInbox.setID(idInt);
		newInbox.setMessage(atts[1].getTextContent());
		return newInbox;
	}

	// builds new coupon bean by element for the getCoupon method. // TODO
	private static Coupon buildNewCouponByElement( String id ){
		Coupon newCoupon = new Coupon();
		Document doc = readFromXMLFile(XmlDB.COUPON_XMLFILE);
		NodeList coupons = doc.getDocumentElement().getChildNodes();
		Node currentCoupon = null;
		Element temp = null;
		for (int i = 0; i < coupons.getLength(); i++) {
			if (coupons.item(i).getNodeType() == Node.ELEMENT_NODE ) {
				temp = (Element) coupons.item(i);
				if (temp.getAttribute("id").equals(id)){
					currentCoupon = coupons.item(i);
					break;
				}
			}
		}
		Node[] atts = new Node[9];
		NodeList attList = currentCoupon.getChildNodes();
		for (int i = 0; i < attList.getLength(); i++) {
			atts[i] = attList.item(i);
		}
		int idInt = Integer.parseInt(id);
		int amount = Integer.parseInt(atts[4].getTextContent());
		String date = atts[2].getTextContent();
		String endDate = atts[3].getTextContent();	
		double price = Double.parseDouble(atts[6].getTextContent());
		newCoupon.setTitle(atts[0].getTextContent());	
		newCoupon.setID(idInt);
		newCoupon.setStartDate(java.sql.Date.valueOf(date));
		newCoupon.setEndDate(java.sql.Date.valueOf(endDate));
		newCoupon.setAmount(amount);
		newCoupon.setMessage(atts[5].getTextContent());
		newCoupon.setPrice(price);
		newCoupon.setImage(atts[7].getTextContent());
		newCoupon.setType(CouponType.valueOf(atts[8].getTextContent()));
		return newCoupon;

	} 

	// removes the given coupon and update the XMl file.
	public static void removeAccessory( Accessory accessory, String filename, String accessoryTpe) {
		if (isXMLEmpty(filename))
			return; // TODO : throw exception
		Document doc = readFromXMLFile(filename); 
		if (getAccessory(accessory.getID(), filename, accessoryTpe) == null) {
			System.out.println("Coupon doesnt exsits");
			return;
		}
		String id = Integer.toString(accessory.getID());
		Element root = doc.getDocumentElement();
		NodeList accessoriess = root.getChildNodes();
		Element temp = null;
		for (int i = 0; i < accessoriess.getLength(); i++) {
			if (accessoriess.item(i).getNodeType() == Node.ELEMENT_NODE ) {
				temp = (Element) accessoriess.item(i);
				if (temp.getAttribute("id").equals(id)){
					temp.getParentNode().removeChild(temp);
					break;
				}
			}
		}
		doc.normalize();
		writeToXMLFile(doc,filename);
	}

	// returns all of the coupons in a coupon collection from the xml file 
	public static Collection<Accessory> getAllAccessories( String filename, String accessoryType ) {
		if (isXMLEmpty(filename))
			return null;
		Document doc = readFromXMLFile(filename);
		Element accessoriess = doc.getDocumentElement();
		NodeList accessoryList = accessoriess.getChildNodes();
		Accessory newAccessory = null;
		Collection<Accessory> collection = new HashSet<>();
		for (int i = 0; i < accessoryList.getLength(); i++) {
			Node accessory = accessoryList.item(i);
			newAccessory = getAccessory( Integer.parseInt(accessory.getAttributes().item(0).getTextContent()), filename, accessoryType );
			collection.add(newAccessory);
		}
		return collection;
	}


}

