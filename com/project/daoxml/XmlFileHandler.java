package com.project.daoxml;

import java.io.File;
import java.io.IOException;

import com.project.constants.XmlDB;
import com.project.daofile.LogLevel;
import com.project.logs.Logger;

/**
 * Manipulates XMLDB files
 * */
public class XmlFileHandler {

	
	public static boolean purgeXmlDBFiles() {
		return createXmlDBFiles();
	}
	
	public static boolean deleteXmlDBFiles() {
		for (int i = 0; i < XmlDB.XML_FILES.length; i++) {
			File file = new File(XmlDB.XML_FILES[i]);
			if ( file.exists() )
				if ( !file.delete() ) {
					Logger.log("could not delete file " + XmlDB.XML_FILES[i], LogLevel.ERROR);
					return false;
				}
		}
		return true;
	}
	
	public static boolean createXmlDBFiles() {
		File xmldir = new File(XmlDB.XMLDB_DIR);
		if ( ! xmldir.exists() )
			if ( !xmldir.mkdirs() )
				return false;
	
		for (int i = 0; i < XmlDB.XML_FILES.length; i++) {
			File file = new File(XmlDB.XML_FILES[i]);
			if ( ! file.exists() ) {
				try {
					file.createNewFile();
				} catch (IOException e) {
					Logger.log("could not create xml db file " + XmlDB.XML_FILES[i], LogLevel.ERROR);
					return false;
				}
			}
		}
		return true;
	}
	
	public static boolean xmlFileDbExists( String xmlFilename ) {
		if ( xmlFilename == null)
			return false;
		
		File xmlFile = new File(xmlFilename);
		if ( xmlFile.exists() )
			return true;
		return false;
	}
	
	public static boolean xmlAllDbFilesExist() {
		for (int i = 0; i < XmlDB.XML_FILES.length; i++)
			if ( ! xmlFileDbExists(XmlDB.XML_FILES[i]) )
				return false;
		return true;
	}
	
	
}
