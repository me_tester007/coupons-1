package com.project.beans;

/**
 * Enumeration for Coupon Type
 * */
public enum CouponType {
	
	UNDEFINED, RESTAURANT, ELECTRICITY, FOOD, HEALTH, SPORTS, CAMPING, TRAVELLING

}
