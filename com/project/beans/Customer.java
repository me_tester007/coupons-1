package com.project.beans;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;

import com.project.exceptions.IllegalCustomerIdException;
import com.project.exceptions.IllegalEmailException;
import com.project.exceptions.IllegalPasswordException;
import com.project.utils.EmailUtils;
import com.project.utils.PasswordUtils;

/**
 * This Java Bean class represents any specific Customer<br>that would acquire Coupons
 * for different purposes<p>
 * Its attributes are being saved/read into/from  DB
 * 
 * */
public class Customer implements Owner {

	private int _id;
	private String _custName;
	private String _password;
	private String _email;
	private Collection<Coupon> _coupons;
	private Collection<Inbox> _inbox;
	
	public Customer() {
		_coupons = new LinkedList<>();
		_inbox = new HashSet<>();
	}
	
	public Customer( int id, String name, String pwd, String email, Collection<Coupon> coupons, Collection<Inbox> inbox ) {
		setID(id);
		setName(name);
		setPassword(pwd);
		setEmail(email);
		setCoupons(coupons);
		setInboxMessages(inbox);
	}
	
	/*
	 * 		GETTERS
	 * */
	@Override
	public int getID() {
		return _id;
	}
	
	@Override
	public String getName() {
		return _custName;
	}
	
	@Override
	public String getPassword() {
		return _password;
	}
	
	@Override
	public String getEmail() {
		return _email;
	}
	
	@Override
	public Collection<Coupon> getCoupons() {
		return Collections.unmodifiableCollection(_coupons);
	}
	
	@Override
	public Collection<Inbox> getInboxMessages() {
		return Collections.unmodifiableCollection(_inbox);
	}
	
	/*
	 * 		SETTERS
	 * */
	@Override
	public void setID( int id ) throws IllegalCustomerIdException {
		if ( id <= 0 )
			throw new IllegalCustomerIdException("Customer ID is illegal");
		_id = id;
	}
	
	@Override
	public void setName( String custName ) {
		if( custName == null )
			_custName = "Undefined";
		else
			_custName = custName;
	}
	
	@Override
	public void setPassword( String password ) throws IllegalPasswordException {
		if ( ! PasswordUtils.isPwdLegal(password) )
			throw new IllegalPasswordException("Password does not match the minimum policy requirement");
		_password = password;
	}
	
	@Override
	public void setEmail( String email ) throws IllegalEmailException {
		if ( ! EmailUtils.isEmailAddressLegal(email) )
			throw new IllegalEmailException("Invalid email recieved");
		_email = email;
	}
	
	@Override
	public void setCoupons( Collection<Coupon> coupons ) {
		if ( coupons == null ){
			if( _coupons == null )
				_coupons = new LinkedList<>();
		} else {
			_coupons = coupons;
		}
	}

	@Override
	public void setInboxMessages( Collection<Inbox> inbox ) {
		if ( inbox == null ) {
			if ( _inbox == null )
				_inbox = new HashSet<>();
		} else {
			_inbox = inbox;
		}
	}
	
	@Override
	public boolean equals( Object obj ) {
		if ( obj == null ) return false;
		if ( obj == this ) return true;
		if ( ! (obj instanceof Customer) )
			return false;
		return _id == ((Customer)obj)._id;
	}
	
	@Override
	public String toString() {
		return "Customer [_id=" + _id + ", _custName=" + _custName + ", _password=" + _password + ", _coupons="
				+ _coupons + ", _inbox=" + _inbox + "]";
	}
}
