package com.project.logs;

import java.sql.Date;

import com.project.beans.Inbox;
import com.project.constants.LogDetails;
import com.project.dao.InboxDao;
import com.project.dao.LogDao;
import com.project.daofile.LogFileDao;
import com.project.daofile.LogLevel;
import com.project.daofile.LogType;
import com.project.exceptions.file.FileRunTimeException;
import com.project.exceptions.sql.SqlRunTimeException;
import com.project.utils.FactoryDao;
import com.project.utils.FileExceptionUtils;
import com.project.utils.InboxUtils;
import com.project.utils.LogUtils;
import com.project.utils.SqlExceptionUtils;

/**
 *  Custom Logger class for handling logs of coupon management server 
 * */
public class Logger {

	private Logger() {}

	/**
	 * Log the operation into general Log file
	 * @return void
	 * */
	public static void log( Log log, LogLevel logLevel ) {
		if ( log == null )
			return;

		if ( ! _approveLogging( logLevel ) )
			return;

		LogDao logDao = LogFileDao.getInstance();

		try {
			logDao.createLog(log, logLevel);
		} catch (FileRunTimeException e) {
			FileExceptionUtils.fileException(e);
		}
	}

	/**
	 * Log the opertaion into genrel log file
	 * @return void
	 * */
	public static void log( String msg, LogLevel logLevel ) {
		if ( msg == null )
			return;

		if ( ! _approveLogging( logLevel ) )
			return;

		//Using directly java.util.Date since sql.Date does not support
		//parameterless constructor and requires 'long' value type as an argument
		long ms = new java.util.Date().getTime();
		log( new Log(0, new Date(ms), msg), logLevel );
	}

	/**
	 * Log the operation into specific LogType file
	 * */
	public static void log( Log log, LogType logType, LogLevel logLevel ) {
		if ( log == null )
			return;

		if ( ! _approveLogging( logLevel ) )
			return;

		LogDao logDao = LogFileDao.getInstance();
		//LogDao logDao = FactoryDao.getLogDao(); ALWAYS RETURN NULL

		try {
			((LogFileDao)logDao).createSpecificLog(log, logType, logLevel);
		} catch (FileRunTimeException e) {
			FileExceptionUtils.fileException(e);
		}
	}

	/**
	 * Log the operation into specific LogType file
	 * */
	public static void log( String msg, LogType logType, LogLevel logLevel ) {
		if ( msg == null )
			return;

		if ( ! _approveLogging( logLevel ) )
			return;
		
		//Using directly java.util.Date since sql.Date does not support
		//parameterless constructor and requires 'long' value type as an argument
		long ms = new java.util.Date().getTime();
		log( new Log(0, new Date(ms), msg), logType, logLevel );
	}

	/**
	 * Log the operation into DB log table
	 * */
	public static void dbLog( Log log, LogLevel logLevel ) {
		if ( log == null )
			return;
		
		if ( ! _approveLogging( logLevel ) )
			return;

		//LogDao logDao = new LogDBDao();
		LogDao logDao = FactoryDao.getLogDao();
		try {
			logDao.createLog(log, LogLevel.INFO);
		} catch (SqlRunTimeException e) {
			SqlExceptionUtils.logException(e);
		}
	}

	/**
	 * Log the operation into DB log table
	 * */
	public static void dbLog( String msg, LogLevel logLevel ) {
		if ( msg == null )
			return;
		
		if ( ! _approveLogging( logLevel ) )
			return;

		//Using directly java.util.Date since sql.Date does not support
		//parameterless constructor and requires 'long' value type as an argument
		long ms = new java.util.Date().getTime();
		dbLog( new Log(LogUtils.getNextID(), new Date(ms), msg), logLevel );
	}

	/**
	 * Log the operation into DB inbox table
	 * */
	public static void inboxLog( Inbox inbox ) {
		if ( inbox == null )
			return;

		//InboxDao inboxDao = new InboxDBDao();
		InboxDao inboxDao = FactoryDao.getInboxDao();
		inboxDao.createInboxMessage(inbox);
	}

	/**
	 * Log the operation into DB log table
	 * */
	public static void inboxLog( String msg ) {
		if ( msg == null )
			return;

		inboxLog( new Inbox(InboxUtils.getNextID(), msg) );
	}

	/**
	 *  Assign specific inbox message to the owner (company/customer)<br/>
	 *  by creating row in JOIN table
	 * */
	public static void updateOwnerInbox( int ownerID, int inboxID, String ownerTable ) {
		if ( ownerID <= 0 || inboxID <= 0 )
			return;

		//InboxDao inboxDao = new InboxDBDao();
		InboxDao inboxDao = FactoryDao.getInboxDao();
		inboxDao.assignInboxToOwner(ownerID, inboxID, ownerTable);
	}

	/*
	 * Verify WARNING AND DEBUG levels are configured before logging them into files 
	 * */
	private static boolean _approveLogging( LogLevel logLevel ) {

		switch ( logLevel ) {
		case WARNING:
			if( LogDetails.currentLogLevel != LogLevel.WARNING )
				return false;
			return true;
		
		case DEBUG:
			if ( LogDetails.currentLogLevel != LogLevel.DEBUG )
				return false;
			return true;
		// other cases as INFO/ERROR/FATAL are logged always
		default: return true;
		}
	}
	
	
}
