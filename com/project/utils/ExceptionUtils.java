package com.project.utils;

import com.project.daofile.LogLevel;
import com.project.exceptions.CouponNotExistException;
import com.project.exceptions.DuplicateCouponException;
import com.project.exceptions.NegativeAmountException;
import com.project.logs.Logger;

/**
 *  Class that handles the general exceptions that was caught during runtime
 * */
public class ExceptionUtils {
	
	
	/**
	 * Handles the exeption which is thrown when the amount of coupons is zero
	 * @return void
	 * */
	public static void absentInStock ( NegativeAmountException exception ) {
		System.err.println("No coupon is available");
	}
	
	/**
	 * Handles the exeption which is thrown when the is NO coupon in db table
	 * */
	public static void couponNotExist( CouponNotExistException exception ) {
		System.err.println(exception.getCoupon().getID() + " does not exist");
	}
	
	public static void duplicateCoupon( DuplicateCouponException exception ) {
		System.err.println(exception.getCouponID() + " does not exist");
	}
	
	/**
	 * Handles the exeption which is thrown when the dayli task is interrupted
	 * */
	public static void dayliTaskInterrupted( InterruptedException exception ) {
		System.err.println("dayli coupon task interrupt issue: " + exception.getMessage());
		Logger.log("dayli coupon task interrupt issue: " + exception.getMessage(), LogLevel.ERROR);
	}
	
	
	
}
