package com.project.utils;

import com.project.dao.CompanyDao;

/**
 *Utils class is used for generating the next ID for company by pulling the </br>
 *last row from the company DB table and incrementing it by one for further usage </br>
 *and initialized only once when server starts
 * */
public class CompanyUtils {

	//private static boolean startup = true;
	private static int _idCounter;
	
	static {
		initializeIdCounter();
	}
	
	
	private CompanyUtils(){}
	
	private static void initializeIdCounter() {
		
		//CompanyDao compDao = new CompanyDBDao();
		CompanyDao companyDao = FactoryDao.getCompanyDao();
		
		if ( (_idCounter = companyDao.getCompanyLatestID()) <= 0 )
			_idCounter = 10000;
	}

	/**
	 * Get next ID for Company object
	 * @return int
	 */
	public static int getNextID(){
		return ++_idCounter;
	}
	
}
