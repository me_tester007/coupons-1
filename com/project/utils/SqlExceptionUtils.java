package com.project.utils;

import com.project.constants.SqlDB;
import com.project.daofile.LogLevel;
import com.project.exceptions.sql.OwnerSqlException;
import com.project.exceptions.sql.SqlRunTimeException;
import com.project.logs.Logger;

/**
 *  This class should handle the SQL exceptions that were caught
 * */
public class SqlExceptionUtils {
	
	public static void tableCreationFailed( SqlRunTimeException exception ) {
		applyTheReason("DB Table creation failed: ", exception);
	}
	
	public static void connectionFail( SqlRunTimeException exception ) {
		applyTheReason("DB connection error: ", exception);
	}
	
	/**
	 * Handle Company exceptions
	 * */
	public static void companyException( OwnerSqlException exception ) {
		applyTheReason("Company", exception);
	}
	
	/**
	 * Handle Customer exceptions
	 * */
	public static void customerException( OwnerSqlException exception ) {	
		applyTheReason("Customer", exception);
	}
	
	/**
	 * Handle Coupons exceptions
	 * */
	public static void couponException( SqlRunTimeException exception ) {
		applyTheReason("Coupon", exception);
	}

	/**
	 * Handle log exceptions
	 * */
	public static void logException( SqlRunTimeException exception ) {
		applyTheReason("Logs", exception);
	}
	
	/**
	 * Handle inbox exceptions
	 * */
	public static void inboxException( SqlRunTimeException exception ) {
		applyTheReason("InboxMSG", exception);
	}
	
	/*
	 * Auxiliary function that determines the reason of the exception 
	 * */
	private static void applyTheReason( String theSource, SqlRunTimeException exception ) {
		StringBuilder reason = new StringBuilder(theSource + " DB operation failed: ");
		reason.append(verifyErrorCode(exception));
		System.out.println(reason);
		//we skip logging the reason if the reason is logs db failure itself
		if ( !theSource.equals("Logs") )
			Logger.log(reason.toString(), LogLevel.ERROR);
	}
	
	/*
	 * Auxiliary funtion that verifies error code of the exception
	 * */
	private static String verifyErrorCode( SqlRunTimeException exception ) {
		
		switch (exception.getErrorCode()) {

		case SqlRunTimeException.SQL_ERROR:
			if ( exception.getMessage().contains("connection closed") )
				return "Connection to Database closed";
			else
				return exception.getMessage();

		case SqlRunTimeException.FAIL_TO_INSERT:
			if ( exception.getMessage().contains("Duplicate") )
				return " object already exists";
			if ( exception.getMessage().contains("child row") )
				return " object does not exist";
			

		case SqlRunTimeException.UPDATE_FAILED:
			return " update failed " + exception.getMessage();

		case SqlRunTimeException.REMOVE_FAILED:
			return " remove failed " + exception.getMessage();

		case SqlRunTimeException.CONNECT_FAIL:
			if ( exception.getMessage().contains("Access denied") )
				return " WRONG DB credentials";
			else if ( exception.getMessage().contains("No suitable driver") )
				return exception.getMessage();
			else if ( exception.getMessage().contains("UnknownHost") )
				return " the host is unknown " + SqlDB.URL;
			return " The Conection to DB failed " + SqlDB.URL + "\nTry switching to dbInstallationType mode in property configuration";
			
		case SqlRunTimeException.CLASS_LOAD_FAIL:
			return " class load failed " + exception.getDriverName();
			
			
		default:
			return exception.getMessage();
		}
	}
	
}
