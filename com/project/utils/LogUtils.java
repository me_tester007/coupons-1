package com.project.utils;

import com.project.dao.LogDao;

	//		DEPRECATED SHOULD NOT BE IN USE
	//		SINCE ALL LOGS ARE WRITTEN IN LOG FILES

/**
 *This class is used for generating the next ID for log objects by pulling the </br>
 *last row from the log DB table and incrementing it by one for further usage </br>
 *and initialized only once when server starts
 * */
public class LogUtils {
	
	private static int _idCounter;
	
	static {
		initializeIdCounter();
	}
	
	private LogUtils(){}
	
	private static void initializeIdCounter() {
	
		//LogDao logDao = new LogDBDao();
		LogDao logDao = FactoryDao.getLogDao();
		
		if ( (_idCounter = logDao.getLogLatestID()) <= 0 )
			_idCounter = 0;
	}
	
	public static int getNextID(){
		return ++_idCounter;
	}


}
