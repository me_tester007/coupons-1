package com.project.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.project.constants.Email;

/**
 * Class is used for validating the emails structure validity using regular expressions
 */
public class EmailUtils {

	private static Pattern pattern = Pattern.compile( Email.PATTERN );
	
	private EmailUtils(){}
	
	/**
	 * Validates email's structure
	 * */
	public static boolean isEmailAddressLegal( final String hex ) {
		Matcher matcher = pattern.matcher(hex);
		return matcher.matches();
	}
}
