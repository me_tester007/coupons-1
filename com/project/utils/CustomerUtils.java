package com.project.utils;

import com.project.dao.CustomerDao;

/**
 *This class is used for generating the next ID for customer by pulling the </br>
 *last row from the customer DB table and incrementing it by one for further usage </br>
 *and initialized only once when server starts
 * */
public class CustomerUtils {
	
	//private static boolean startup = true;
	private static int _idCounter;
	
	static {
		initializeIdCounter();
	}
	
	private CustomerUtils() {}

	private static void initializeIdCounter() {

		//CustomerDao custDao = new CustomerDBDao();
		CustomerDao customerDao = FactoryDao.getCustomerDao();
		
		if ( (_idCounter = customerDao.getCustomerLatestID()) <= 0 )
			_idCounter = 100;
	}
	
	/**
	 * Get next ID for Customer object
	 * @return int
	 * */
	public static int getNextID(){
		return ++_idCounter;
	}
	
}
