package com.project.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.project.constants.FileNames;
import com.project.daofile.LogLevel;
import com.project.exceptions.GenRunTimeException;
import com.project.exceptions.file.FileRunTimeException;
import com.project.logs.Logger;
import com.project.tasks.PropertyFileBuilder;

/**
 * Class that loads on startup and reloads db type from property file
 * */
public class PropertiesValidator {

	private static Properties _properties = null;
	private static InputStream _inputStream = null;

	private PropertiesValidator(){}

	/**
	 * Get the DB type and details 
	 * */
	public static Properties getInitialParameters() throws FileRunTimeException, GenRunTimeException {

		_properties = new Properties();
		try {
			_inputStream = new FileInputStream(FileNames.PROPERTY_FILE_NAME);
			_properties.load(_inputStream);
		} catch (FileNotFoundException e) {
			PropertyFileBuilder.invokePropertyCreation();
			getInitialParameters();
		} catch (IOException e) {
			throw new FileRunTimeException(e, FileRunTimeException.IO_ERR);
		} catch ( IllegalArgumentException e ) {
			throw new GenRunTimeException(e, GenRunTimeException.ILLEGAL_ARG);
		} finally {
			try {
				if ( _inputStream != null )
					_inputStream.close();
			} catch (IOException e) {
				throw new FileRunTimeException(e, FileRunTimeException.IO_ERR);
			}
		}
		return _properties;
	}
	
	public static void closePropertyFile() {
		try {
			if ( _inputStream != null )
				_inputStream.close();
		} catch (IOException e) {
			Logger.log("Error closing main propert file: ", LogLevel.ERROR);
		}
	}
	
}
