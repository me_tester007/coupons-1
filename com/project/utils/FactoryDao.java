package com.project.utils;

import java.util.Properties;

import com.project.CouponSystem;
import com.project.dao.CompanyDao;
import com.project.dao.CouponDao;
import com.project.dao.CustomerDao;
import com.project.dao.InboxDao;
import com.project.dao.LogDao;
import com.project.daofile.LogLevel;
import com.project.logs.Logger;

/**
 * Generic Class that dynamically creates dao objects based on DB type chosen
 * */
public class FactoryDao {

	private static String _classname = null;
	private static String _companyClass = null;
	private static String _customerClass = null;
	private static String _couponClass = null;
	private static String _inboxClass = null;

	private static CompanyDao _companyDao = null;
	private static CustomerDao _customerDao = null;
	private static CouponDao _couponDao = null;
	private static InboxDao _inboxDao = null;
	//private static final CompanyDao _logDao = new CompanyDBDao();

	private FactoryDao() {}

	public static String getClassname() {
		return _classname;
	}
	
	/**
	 * Initializes the db type details on server startup
	 * */
	public static void initFactoryDao( Properties properties ) {
		String dbType = null;
		if ( (dbType = properties.getProperty("dbType")) == null ){
			Logger.log("Error db type unknown ", LogLevel.ERROR);
			System.exit(1);
		}
	
		 _classname = properties.getProperty("classname");
		
		if ( (dbType.equalsIgnoreCase("xml")) && ! _classname.contains("xml") ) {
			System.err.println("System SHUTDOWN\nCheck LOGS");
			Logger.log("Configuration missmatched xml + sql in properties file", LogLevel.ERROR);
			System.exit(1);
		}
		
		_initClassName(_classname);
		_initDaoObjects(_classname);
	}

	/**
	 * Returns single instance of company dao object 
	 * */
	public static CompanyDao getCompanyDao() {
		return _companyDao;
	}

	/**
	 * Returns single instance of customer dao object 
	 * @return {@link CustomerDao}
	 * */
	public static CustomerDao getCustomerDao() {
		return _customerDao;
	}

	/**
	 * Returns single instance of coupon dao object
	 * @return {@link CouponDao}
	 * */
	public static CouponDao getCouponDao() {
		return _couponDao;
	}

	/**
	 * Returns single instance of inbox dao object
	 * @return {@link InboxDao}
	 * */
	public static InboxDao getInboxDao() {
		return _inboxDao;
	}

	/**
	 * Returns single instance of log dao object 
	 * */
	public static LogDao getLogDao() {
		// NOT IN USE
		return null;
	}

	// initializes full classpath for all dao objects
	private static void _initClassName( String classname ) {
		_companyClass = classname + ".CompanyDBDao";
		_customerClass = classname + ".CustomerDBDao";
		_couponClass = classname + ".CouponDBDao";
		_inboxClass = classname + ".InboxDBDao";
	}

	// initializes the instances of all dao objects
	private static void _initDaoObjects( String classname ) {
		try {
			_companyDao = (CompanyDao)Class.forName(_companyClass).newInstance();
			_customerDao = (CustomerDao)Class.forName(_customerClass).newInstance();
			_couponDao = (CouponDao)Class.forName(_couponClass).newInstance();
			_inboxDao = (InboxDao)Class.forName(_inboxClass).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			Logger.log(classname + " class load fatal error", LogLevel.FATAL);
			System.err.println("class load error: " + classname + " Shutting Down");
			if ( CouponSystem.getInstance() != null )
				CouponSystem.getInstance().shutdown();
			System.exit(1);
		}
	}

}
