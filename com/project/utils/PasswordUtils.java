package com.project.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.project.constants.Password;

	/**
	 * Class uses one static function to verify if password matches minimum 
	 * policy requirement 
	 * */
public class PasswordUtils {

	private static Pattern pattern = Pattern.compile( Password.PATTERN );

	private PasswordUtils(){}

	/**
	 * Validate password with regular expression
	 * @return boolean
	 */
	public static boolean isPwdLegal( final String password ){
		Matcher matcher = pattern.matcher(password);
		return matcher.matches();
	}
}
