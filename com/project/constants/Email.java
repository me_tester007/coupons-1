package com.project.constants;

/**
 * Minimum requirement policy for email verification
 * */
public class Email {

	private Email() {}

	//---EMAILS POLICY
	public static final String PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
					+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

}
