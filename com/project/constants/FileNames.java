package com.project.constants;

/**
 * Names of the full Path of the files
 * */
public class FileNames {

	// Logs related INFO
	public static final String LOGS_DIR = "logs";
	public static final String COUPONSYS_ALL = LOGS_DIR + "/coupon_systemAll.log";
	public static final String COUPON = LOGS_DIR + "/coupon.log";
	public static final String CUSTOMER = LOGS_DIR + "/customer.log";
	public static final String COMPANY = LOGS_DIR + "/company.log";
	public static final String INBOX = LOGS_DIR + "/inbox.log";

	// Application PROPERTY FILE
	public static final String PROPERTY_DIR = "conf";
	public static final String PROPERTY_FILE_NAME = PROPERTY_DIR + "/config.properties";
	
	private FileNames() {}
	
}
