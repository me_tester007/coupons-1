package com.project.constants;

/**
 * Minimum requirement policy for password verification
 * */
public class Password {

	public Password() {}
	
	//---PASSWORDS POLICY
	public static final int MIN_LENGTH = 5;
	public static final int MAX_LENGTH = 10;	
	public static final String PATTERN = 
			"((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{" +
					MIN_LENGTH + "," + MAX_LENGTH + "})";


}
