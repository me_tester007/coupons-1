package com.project.constants;

/**
 * Constants for JDBC POOL SIZE
 * */
public class JdbcPool {

	private JdbcPool() {}

	//---Connection Pool Sizes
	public static final int INIT_SIZE = 2;
	public static final int MAX_SIZE = 10;
	
}
