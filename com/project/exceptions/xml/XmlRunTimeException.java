package com.project.exceptions.xml;

import com.project.exceptions.dao.DaoRumTimeException;

public class XmlRunTimeException extends RuntimeException implements DaoRumTimeException {

	private static final long serialVersionUID = 1L;

	public static final int SAX_ERR = 1;
	public static final int IO_ERR = 2;
	public static final int PARSE_ERR = 3;
	public static final int PARSE_CONF_ERR = 4;
	public static final int TRANSFORMER = 5;
	public static final int XPATH_ERROR = 6;
	//public static final int 
	
	private Exception _exception;
	private int _errorType;
	
	public XmlRunTimeException( Exception e, int errType) {
		super(e.getMessage());
		_errorType = errType;
	}

	public XmlRunTimeException( String msg ) {
		super(msg);
	}
	
	public int getErrorType() {
		return _errorType;
	}
	
	public String getExceptionMessage() {
		return _exception.getMessage();
	}
	
}
