package com.project.exceptions;

public class IllegalCompanyNameException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public IllegalCompanyNameException( String msg ){
		super(msg);
	}

}
