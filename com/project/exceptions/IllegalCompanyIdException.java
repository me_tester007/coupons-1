package com.project.exceptions;

public class IllegalCompanyIdException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IllegalCompanyIdException( String msg ) {
		super(msg);
	}

}
