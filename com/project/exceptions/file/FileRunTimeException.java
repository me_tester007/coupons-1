package com.project.exceptions.file;


public class FileRunTimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public static final int IO_ERR = 1;
	public static final int FILE_NOT_FOUND = 2;
	public static final int NULLPOINTER = 3;
	//public static final int
	
	private Exception _exceptioin;
	private int _err;
	
	public FileRunTimeException( String msg, int err ) {
		super(msg);
		_err = err;
	}
	
	public FileRunTimeException( Exception exception, int err ) {
		super(exception.getMessage());
		_exceptioin = exception;
		_err= err;
	}
	
	public String getExceptionMessage() {
		return super.getMessage();
	}
	
	public Exception getException() {
		return _exceptioin;
	}
	
	public int getError() {
		return _err;
	}

}
