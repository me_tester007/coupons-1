package com.project.exceptions;

public class OwnerNotExistException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public OwnerNotExistException( String msg ) {
		super(msg);
	}

}
