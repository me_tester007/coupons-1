package com.project.exceptions;

public class IllegalCustomerIdException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public IllegalCustomerIdException( String msg ) {
		super(msg);
	}

}
