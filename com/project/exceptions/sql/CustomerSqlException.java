package com.project.exceptions.sql;

import java.sql.SQLException;

public class CustomerSqlException extends OwnerSqlException {

	private static final long serialVersionUID = 1L;

	public CustomerSqlException(String msg, int errorCode) {
		super(msg, errorCode);
	}
	
	public CustomerSqlException( String msg ) {
		super(msg);
	}
	
	public CustomerSqlException( SQLException e ) {
		super(e);
	}

}
