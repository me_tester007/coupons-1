package com.project.exceptions.sql;

import java.sql.SQLException;

import com.project.exceptions.dao.DaoRumTimeException;

public class SqlRunTimeException extends RuntimeException implements DaoRumTimeException {
	
	private static final long serialVersionUID = 1L;
	
	public static final int FAIL_TO_INSERT = 1;
	public static final int UPDATE_FAILED = 2;
	public static final int REMOVE_FAILED = 3;
	public static final int CONNECT_FAIL = 4;
	public static final int CLASS_LOAD_FAIL = 5;
	public static final int SQL_ERROR = 6;
	
	private int _errorCode;
	private SQLException _exception;
	private String _driverName;
	
	public SqlRunTimeException(){}
	
	public SqlRunTimeException( String msg, int errorCode ) {
		super(msg);
		_errorCode = errorCode;
	}
	
	public SqlRunTimeException( SQLException exception, int errorCode ) {
		super(exception.getMessage());
		_errorCode = errorCode;
		_exception = exception;
	}
	
	public SqlRunTimeException( String msg ) {
		super(msg);
	}
	
	public SqlRunTimeException( String msg, String driver ) {
		super(msg);
		_driverName = driver;
	}
	
	public SqlRunTimeException( SQLException e ) {
		e.printStackTrace();
	
	}
	
	public String getDriverName() {
		return _driverName;
	}
	
	public int getErrorCode() {
		return _errorCode;
	}
	
	
	public SQLException getSQLException() {
		return _exception;
	}

}
