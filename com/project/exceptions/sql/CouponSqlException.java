package com.project.exceptions.sql;

import java.sql.SQLException;

public class CouponSqlException extends SqlRunTimeException {
	
	private static final long serialVersionUID = 1L;

	public CouponSqlException(String msg, int errorCode) {
		super(msg, errorCode);
	}
	
	public CouponSqlException( String msg ) {
		super(msg);
	}
	
	
	public CouponSqlException( SQLException e ) {
		super(e);
	}

}
