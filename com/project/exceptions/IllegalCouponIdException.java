package com.project.exceptions;

public class IllegalCouponIdException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public IllegalCouponIdException( String msg ) {
		super(msg);
	}
	

}
