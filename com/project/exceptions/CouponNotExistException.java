package com.project.exceptions;

import com.project.beans.Coupon;

public class CouponNotExistException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private Coupon _coupon;
	
	public CouponNotExistException( Coupon coupon ) {
		super("Coupon does not exist " + coupon.getID());
		_coupon = coupon;
	}
	
	public Coupon getCoupon() {
		return _coupon;
	}
}
