package com.project.exceptions;

public class DuplicateCouponException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private int _couponID;
	private Exception _exception;
	
	public DuplicateCouponException( Exception exception, int couponID ) {
		super(exception.getMessage());
		_exception = exception;
		_couponID = couponID;
	}
	
	public String getExceptionMessage() {
		return _exception.getMessage();
	}
	
	public Exception getException() {
		return _exception;
	}
	
	public int getCouponID() {
		return _couponID;
	}

}
