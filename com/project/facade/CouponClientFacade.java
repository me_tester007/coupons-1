package com.project.facade;

/**
 * Interface for Admin/Customer/Company objects
 * */
public interface CouponClientFacade {

	public CouponClientFacade login( String name, String pwd, ClientType clientType );
}
