package com.project.facade;

/**
 * This Class represents the client type of the Coupon System<br/>
 * Which can be Adminisrator, Company or Customer client
 * */
public enum ClientType {

	UNDEFINED, ADMIN, COMPANY, CUSTOMER
	
}
