package com.project.daosql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;

import com.project.beans.Inbox;
import com.project.constants.SqlDB;
import com.project.dao.InboxDao;
import com.project.exceptions.sql.InboxSqlException;
import com.project.jdbc.ConnectionPool;

/**
 * DAO DataBase Layer Class working with MySQL DB server
 * */
public class InboxDBDao implements InboxDao {

	private ConnectionPool _pool;
	
	/**
	 * parameterless Class Consturtor that initializes _pool variable to receive<br/>
	 * the ConnectionPool Singleton instance
	 * */
	public InboxDBDao() {
		_pool = ConnectionPool.getInstance();
	}
	
	/**
	 * Create/Send message into inbox db table
	 * @return void
	 * */
	@Override
	public void createInboxMessage( Inbox inbox ) throws InboxSqlException {
		if ( inbox == null )
			return;

		String query = " insert into " + SqlDB.INBOX_TABLE + " () values (?,?)";
		PreparedStatement ps = null;
		Connection _conn = null;

		try {
			_conn = _pool.getConnection();
			_conn.setAutoCommit(false);
			ps = _conn.prepareStatement(query);
			ps.setInt(1, inbox.getID());
			ps.setString(2, inbox.getMessage());
			ps.execute();
			_conn.commit();
		} catch ( SQLException e ) {
			if (_conn != null) // check if still have connection initialized and try rolling back
				try {
					_conn.rollback();
				} catch (SQLException e1) {
					throw new InboxSqlException(e1.getMessage(), InboxSqlException.SQL_ERROR);
				}
			throw new InboxSqlException(e.getMessage(), InboxSqlException.FAIL_TO_INSERT);
		} finally {
			try {
				if ( ps != null )
					ps.close();
				_conn.setAutoCommit(true);
			} catch (SQLException e) {
				throw new InboxSqlException(e.getMessage(), InboxSqlException.SQL_ERROR);
			}//by closing prepared statement "ps" - result set "rs" is closed automatically
			_pool.returnConnection(_conn);
		}

	}

	/**
	 * Remove inbox message from inbox table
	 * @return void
	 * */
	@Override
	public void removeInboxMessage( Inbox inbox ) throws InboxSqlException {
		if ( inbox == null )
			return;
		
		String query = " delete from " + SqlDB.INBOX_TABLE + " where id=" + inbox.getID();
		Connection _conn = null;
		PreparedStatement ps = null;
		try {
			_conn = _pool.getConnection();
			_conn.setAutoCommit(false);
			ps = _conn.prepareStatement(query);
			ps.execute();
			_conn.commit();
		} catch ( SQLException e ) {
			if (_conn != null) {
				try {
					_conn.rollback();
				} catch (SQLException e1) {
					throw new InboxSqlException(e1.getMessage(), InboxSqlException.SQL_ERROR);
				}
				throw new InboxSqlException(e.getMessage(), InboxSqlException.REMOVE_FAILED);
			}
		} finally {
			try {
				if ( ps != null )
					ps.close();
				_conn.setAutoCommit(true);
			} catch (SQLException e) {
				throw new InboxSqlException(e.getMessage(), InboxSqlException.SQL_ERROR);
			}
			_pool.returnConnection(_conn);
		}
	}

	/**
	 * Update any given inbox message in inbox db table
	 * @return void
	 * */
	@Override
	public void updateInboxMessage( Inbox inbox ) throws InboxSqlException {
		if ( inbox == null )
			return;

		PreparedStatement ps = null;
		String query = " update " + SqlDB.INBOX_TABLE + " set message=? where id=?";
		Connection _conn = null;

		try {
			_conn = _pool.getConnection();
			_conn.setAutoCommit(false);
			ps = _conn.prepareStatement(query);
			ps.setString(1, inbox.getMessage());
			ps.setInt(2, inbox.getID());
			ps.execute();
			_conn.commit();
		} catch (SQLException e) {
			if ( _conn != null )
				try {
					_conn.rollback();
				} catch (SQLException e1) {
					throw new InboxSqlException(e1.getMessage(), InboxSqlException.SQL_ERROR);
				}
			throw new InboxSqlException(e.getMessage(), InboxSqlException.UPDATE_FAILED);
		} finally {
			try {
				if ( ps != null )
					ps.close();
				_conn.setAutoCommit(true);
			} catch (SQLException e) {
				throw new InboxSqlException(e.getMessage(), InboxSqlException.SQL_ERROR);
			}
			_pool.returnConnection(_conn);
		}
	}

	/**
	 * Retreive any given inbox message by its id
	 * @return {@link Inbox}
	 * */
	@Override
	public Inbox getInboxMessage( int id ) throws InboxSqlException {
		if ( id <= 0 )
			return null;
		
		Inbox inboxMsg = null;
		String query = " select * from " + SqlDB.INBOX_TABLE + " where id=" + id;
		Connection _conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			_conn = _pool.getConnection();
			st = _conn.createStatement();
			rs = st.executeQuery(query);
			if ( rs.next() )
				inboxMsg = new Inbox(rs.getInt(1), rs.getString(2));
		} catch ( SQLException e ) {
			throw new InboxSqlException(e.getMessage(), InboxSqlException.SQL_ERROR);
		} finally {
			try {
				if ( st != null )
					st.close();
			} catch (SQLException e) {
				throw new InboxSqlException(e.getMessage(), InboxSqlException.SQL_ERROR);
			}
			_pool.returnConnection(_conn);
		}
		return inboxMsg;
	}

	/**
	 * Retreive all Inbox Messages from inbox db table
	 * @return {@link Inbox} Collection
	 * */
	@Override
	public Collection<Inbox> getAllMessages( ) throws InboxSqlException {
		
		Collection<Inbox> messages = null;
		String query = " select * from " + SqlDB.INBOX_TABLE;
		Statement st = null;
		ResultSet rs = null;
		Connection _conn = null;
		try {
			_conn = _pool.getConnection();
			st = _conn.createStatement();
			rs = st.executeQuery(query);
			messages = deserializeInboxes(rs);
		} catch (SQLException e) {
			throw new InboxSqlException(e.getMessage(), InboxSqlException.SQL_ERROR);
		} finally {
			if ( st != null )
				try {
					st.close();
				} catch (SQLException e) {
					throw new InboxSqlException(e.getMessage(), InboxSqlException.SQL_ERROR);
				}
			_pool.returnConnection(_conn);
		}
		return messages;
	}

	/**
	 * this method is used to get latest index id of inbox message from inbox db table<br/>
	 * in order to continue id numbering<br/>
	 * @return int
	 * */
	@Override
	public int getInboxLatestID() throws InboxSqlException {
		int index = -1;
		Statement st = null;
		ResultSet rs = null;
		
		String query = "SELECT * FROM " + SqlDB.INBOX_TABLE + " order by id DESC limit 1";
		Connection _conn = null;
		
		try {
			_conn = _pool.getConnection();
			st = _conn.createStatement();
			rs = st.executeQuery(query);
			if ( rs.next() )
				index = rs.getInt(1);
		} catch (SQLException e) {
			throw new InboxSqlException(e.getMessage(), InboxSqlException.SQL_ERROR);
		} finally {
			if ( st != null )
				try {
					st.close();
				} catch (SQLException e) {
					throw new InboxSqlException(e.getMessage(), InboxSqlException.SQL_ERROR);
				}
			_pool.returnConnection(_conn);
		}
		return index;
	}

	/**
	 * Sends message to the Owner's (company/customer) inbox table
	 * actually updates the owner's JOIN table only
	 * @return void
	 * */
	@Override
	public void assignInboxToOwner( int ownerID, int inboxID, String joinTable ) throws InboxSqlException {
		if ( ownerID <= 0 || inboxID <= 0 || joinTable == null )
			return;
		
		String query = " insert into " + joinTable + " () values (" + ownerID + "," + inboxID + ")";
		PreparedStatement ps = null;
		Connection _conn = null;
		
		try {
			_conn = _pool.getConnection();
			_conn.setAutoCommit(false);
			ps = _conn.prepareStatement(query);
			ps.execute();
			_conn.commit();
		} catch (SQLException e) {
			if ( _conn != null )
				try {
					_conn.rollback();
				} catch (SQLException e1) {
					throw new InboxSqlException(e1.getMessage(), InboxSqlException.SQL_ERROR);
				}
			throw new InboxSqlException(e.getMessage(), InboxSqlException.FAIL_TO_INSERT);
		} finally {
			try {
				if ( ps != null )
					ps.close();
				_conn.setAutoCommit(true);
			} catch (SQLException e) {
				throw new InboxSqlException(e.getMessage(), InboxSqlException.SQL_ERROR);
			}
			_pool.returnConnection(_conn);
		}
	}
	
	/**
	 * Deletes inbox message from Owner's (cpmany/customer) inbox table
	 * Actually deletes inbox reference from owner's JOIN table
	 * @return void
	 * */
	@Override
	public void removeInboxFromOwner(int inboxID, String joinTable) throws InboxSqlException {
		if ( inboxID <= 0 || joinTable == null )
			return;
		
		String query = " delete from " + joinTable + " where " + SqlDB.INBOXID_COLUMN + "=" + inboxID;
		PreparedStatement ps = null;
		Connection _conn = null;
		
		try {
			_conn = _pool.getConnection();
			_conn.setAutoCommit(false);
			ps = _conn.prepareStatement(query);
			ps.execute();
			_conn.commit();
		} catch (SQLException e) {
			if ( _conn != null )
				try {
					_conn.rollback();
				} catch (SQLException e1) {
					throw new InboxSqlException(e1.getMessage(), InboxSqlException.SQL_ERROR);
				}
			throw new InboxSqlException(e.getMessage(), InboxSqlException.REMOVE_FAILED);
		} finally {
			try {
				if ( ps != null )
					ps.close();
				_conn.setAutoCommit(true);
			} catch (SQLException e) {
				throw new InboxSqlException(e.getMessage(), InboxSqlException.SQL_ERROR);
			}
			_pool.returnConnection(_conn);
		}
	}

	/**
	 * Deletes owner (customer/company) of the specific inbox message
	 * Actually deletes reference from JOIN table by owner's id
	 * */
	@Override
	public void removeInboxOwner(int ownerID, String ownerName, String table) throws InboxSqlException {
		if ( ownerID <= 0 || ownerName == null || table == null )
			return;
		
		String query = " delete from " + table + " where " + ownerName + "=" + ownerID;
		PreparedStatement ps = null;
		Connection _conn = null;
		
		try {
			_conn = _pool.getConnection();
			_conn.setAutoCommit(false);
			ps = _conn.prepareStatement(query);
			ps.execute();
			_conn.commit();
		} catch (SQLException e) {
			if ( _conn != null )
				try {
					_conn.rollback();
				} catch (SQLException e1) {
					throw new InboxSqlException(e1.getMessage(), InboxSqlException.SQL_ERROR);
				}
			throw new InboxSqlException(e.getMessage(), InboxSqlException.REMOVE_FAILED);
		} finally {
			try {
				if ( ps != null )
					ps.close();
				_conn.setAutoCommit(true);
			} catch (SQLException e) {
				throw new InboxSqlException(e.getMessage(), InboxSqlException.SQL_ERROR);
			}
			_pool.returnConnection(_conn);
		}
	}
	
	//Auxiliary function to reconstruct the inbox Collection
	private Collection<Inbox> deserializeInboxes( ResultSet rs ) throws SQLException {
		if ( rs == null )
			return null;
		Collection<Inbox> messages = new HashSet<>();
		while ( rs.next() )
			messages.add( new Inbox(rs.getInt(1), rs.getString(2)) );
		return messages;
	}
	
}
