package com.project.daosql;

import java.util.Collection;
import java.util.HashSet;

import com.project.beans.BeanClassType;
import com.project.beans.Coupon;
import com.project.beans.Customer;
import com.project.beans.Inbox;
import com.project.beans.Owner;
import com.project.constants.SqlDB;
import com.project.dao.CustomerDao;
import com.project.exceptions.GenRunTimeException;
import com.project.exceptions.sql.OwnerSqlException;

/**
 * DAO DataBase Layer Class working with MySQL DB server
 * */
public class CustomerDBDao implements CustomerDao {
	
	/**
	 * parameterless Class Consturtor
	 * */
	public CustomerDBDao() {}
	
	
	/**
	 * Creating 'Customer' entry in the customer DB Table
	 * @return void
	 * */
	@Override
	public void createCustomer( Customer customer ) throws OwnerSqlException {
		if ( customer == null )
			return;
		
		DaoSqlHandler.createOwner(customer, SqlDB.CUSTOMER_TABLE);
	}

	/**
	 * Removing 'Customer' entry from the customer DB Table
	 * @return void
	 * */
	@Override
	public void removeCustomer( Customer customer ) throws OwnerSqlException {
		if ( customer == null )
			return;
		
		DaoSqlHandler.removeOwner(customer, SqlDB.CUSTOMER_TABLE);
	}

	/**
	 * Updating 'Customer' specific entry in the customer DB Table
	 * @return void
	 * */
	@Override
	public void updateCustomer( Customer customer ) throws OwnerSqlException {
		if ( customer == null )
			return;
		
		DaoSqlHandler.updateOwner(customer, SqlDB.CUSTOMER_TABLE);
	}
	
	/**
	 * Requesting specific 'Customer' entry from the customer DB Table using Customer ID
	 * @return {@link Customer}
	 * */
	@Override
	public Customer getCustomer( int id ) throws OwnerSqlException, GenRunTimeException {
		if ( id <= 0 )
			return null;
		
		return (Customer)DaoSqlHandler.getOwner(id, SqlDB.CUSTOMER_TABLE, BeanClassType.CUSTOMER);
	}
	
	/**
	 * Retrieving given customer by its name from customer DB table
	 * @return {@link Customer}
	 * */
	@Override
	public Customer getCustomer( String name ) throws OwnerSqlException, GenRunTimeException {
		if ( name == null )
			return null;
		
		return (Customer)DaoSqlHandler.getOwner(name, SqlDB.CUSTOMER_TABLE, BeanClassType.CUSTOMER);
	}

	/**
	 * Retrieves all available customers from customer DB table
	 * @return {@link Customer} Collection
	 */
	@Override
	public Collection<Customer> getAllCustomers() throws OwnerSqlException, GenRunTimeException {
		
		Collection<Owner> owners = DaoSqlHandler.getAllOwners(SqlDB.CUSTOMER_TABLE, BeanClassType.CUSTOMER);
		Collection<Customer> customers = new HashSet<>();
		for (Owner owner : owners)
			customers.add((Customer)owner);
		
		return customers;
	}

	/**
	 * Retrieves all coupons that belongs to any specific customer<br/>
	 * @return Collection of {@linkplain Coupon}
	 * */
	@Override
	public Collection<Coupon> getCoupons( int customerID ) throws OwnerSqlException {
		if ( customerID <= 0 )
			return null;

		return DaoSqlHandler.getCoupons(customerID, SqlDB.CUSTOMER_COUPON_TABLE, SqlDB.CUSTID_COLUMN);
	}
	
	/**
	 * Retrieves all inbox messages that are related to any specific customer by its id
	 * @return {@link Inbox} Collection
	 * */
	@Override
	public Collection<Inbox> getInboxMessages( int custmerID ) throws OwnerSqlException {
		if ( custmerID <= 0 )
			return null;
		
		return DaoSqlHandler.getInboxMessages(custmerID, SqlDB.CUSTOMER_INBOX_TABLE, SqlDB.CUSTID_COLUMN);
	}

	/**
	 * this function is needed to get latest customer index in order to continue id numbering<br/>
	 * @return int
	 * */
	public int getCustomerLatestID() throws OwnerSqlException {
		
		return DaoSqlHandler.getOwnerLatestID(SqlDB.CUSTOMER_TABLE);
	}
	
	/**
	 * Login function verifies access previliges for any specific customer<br/>
	 * Returns true if credentials are valid - false otherwise
	 * @return boolean
	 * */
	@Override
	public boolean login( String customerName, String password ) throws OwnerSqlException {
		if ( customerName == null || password == null )
			return false;
		
		return DaoSqlHandler.login(customerName, password, SqlDB.CUSTOMER_TABLE);
	}
	
	
}
