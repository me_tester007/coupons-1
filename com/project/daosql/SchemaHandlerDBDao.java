package com.project.daosql;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import com.project.constants.SqlDB;
import com.project.dao.DaoSchemaHandler;
import com.project.daofile.LogLevel;
import com.project.exceptions.sql.CouponSqlException;
import com.project.exceptions.sql.SqlRunTimeException;
import com.project.jdbc.ConnectionPool;
import com.project.logs.Logger;
import com.project.utils.SqlExceptionUtils;

/**
 * Single Class for manipulating DB queries and handling sql related operations
 * ussually used at system startup to create and populate DB with tables
 * */
public class SchemaHandlerDBDao implements DaoSchemaHandler {

	private ConnectionPool _pool = null;

	public SchemaHandlerDBDao() {}

	/**
	 * Create complete new schema in case schema does not exist
	 * and "create" option is configured in system property file
	 * */
	@Override
	public boolean createSchema( String schemaName ) {

		Connection conn = null;
		try {
			Class.forName(SqlDB.DRV_NAME);
			conn = DriverManager.getConnection( SqlDB.getGeneralUrl(), SqlDB.USERNAME, SqlDB.PASSWORD );
		} catch (SQLException e) {
			throw new SqlRunTimeException(e.getMessage(), SqlRunTimeException.CONNECT_FAIL);
		} catch (ClassNotFoundException e) {
			throw new SqlRunTimeException(e.getMessage(), SqlRunTimeException.CLASS_LOAD_FAIL);
		}

		Statement state = null;
		boolean isScriptExecuted = false;
		try {
			state = conn.createStatement();
			state.execute("CREATE DATABASE " + SqlDB.NAME);
			isScriptExecuted = true;
		} catch (Exception e) {
			System.err.println("Failed to Create " + schemaName +". The error is "+ e.getMessage());
			throw new SqlRunTimeException(e.getMessage(), SqlRunTimeException.CONNECT_FAIL);
		} 
		return isScriptExecuted;

	}

	/**
	 * Delete schema from DB server
	 * Currently NOT in USE
	 * */
	@Override
	public boolean deleteSchema(String schemaName) {
		// TODO Auto-generated method stub
		return false;

	}

	@Override
	public boolean createTable(String tableName) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteTable(String tableName) {

		return false;
	}

	@Override
	public void purgeTable(String tableName) {
		// delete from coupons.company_coupon;
		String query = " delete from "+ tableName;
		_pool = ConnectionPool.getInstance();
		PreparedStatement ps = null;
		Connection _conn = null;

		try {
			_conn = _pool.getConnection();
			_conn.setAutoCommit(false);
			ps = _conn.prepareStatement(query);
			ps.execute();
			_conn.commit();
		} catch (SQLException e) {
			if ( _conn != null )
				try {
					_conn.rollback();
				} catch (SQLException e1) {
					throw new CouponSqlException(e1.getMessage(), CouponSqlException.SQL_ERROR);
				}
			throw new CouponSqlException(e.getMessage(), CouponSqlException.REMOVE_FAILED);
		} finally {
			try {
				if ( ps != null )
					ps.close();
				_conn.setAutoCommit(true);
			} catch (SQLException e) {
				throw new CouponSqlException(e.getMessage(), CouponSqlException.SQL_ERROR);
			}
			_pool.returnConnection(_conn);
		}

	}

	@Override
	public void purgeAllTable() {
		for (String tableName : SqlDB.TABLE_NAMES) {
			purgeTable(tableName);
		}

	}

	@Override
	public boolean deleteAllTables( ) {

		boolean success = true;
		for (String tableName : SqlDB.TABLE_NAMES) {
			if ( deleteTable(tableName) == false )
				success = false;
		}
		return success;
	}

	public void initializeDefaultTables( String schemaName ) {
		_createAllDefaultTables(schemaName);
	}
	//------------------------------------------------------

	public boolean createDefaultTable( String sqlScriptFile ) throws SqlRunTimeException {

		_pool = ConnectionPool.getInstance();
		Connection conn = null;
		Statement state = null;
		boolean isScriptExecuted = false;
		try {
			conn = _pool.getConnection();
			state = conn.createStatement();
			BufferedReader in = new BufferedReader(new FileReader(sqlScriptFile));
			String str;
			while ((str = in.readLine()) != null)
				state.executeUpdate(str);
			System.out.println("DB Table Created: " + sqlScriptFile);
			in.close();
			isScriptExecuted = true;
		} catch (Exception e) {
			System.err.println("Failed to Execute" + sqlScriptFile +". The error is"+ e.getMessage());
			throw new SqlRunTimeException(e.getMessage(), SqlRunTimeException.UPDATE_FAILED);
		} finally {
			_pool.returnConnection(conn);
		}
		return isScriptExecuted;
	}

	private boolean _createAllDefaultTables( String schemaName ) {

		int i = 0;
		try {
			for ( i = SqlDB.TABLE_NAMES.length - 1; i >= 0; i-- )
				createDefaultTable(SqlDB.TABLE_SQL_FILES[i]);
		} catch (SqlRunTimeException e) {
			Logger.log("Default Table creation failed: " + SqlDB.TABLE_NAMES[i], LogLevel.FATAL);
			SqlExceptionUtils.tableCreationFailed(e);
			System.exit(1);
		}

		return true;
	}


}
