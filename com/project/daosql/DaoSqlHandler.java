package com.project.daosql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;

import com.project.beans.Coupon;
import com.project.beans.CouponType;
import com.project.beans.Inbox;
import com.project.beans.Owner;
import com.project.constants.SqlDB;
import com.project.exceptions.GenRunTimeException;
import com.project.exceptions.sql.OwnerSqlException;
import com.project.jdbc.ConnectionPool;

/**
* DAO Single Class for manipulating all db related operations
* used by all DBDAO classes
* */
public class DaoSqlHandler {

	private DaoSqlHandler() {}
	
	private static ConnectionPool _pool = ConnectionPool.getInstance();
	
	/**
	 * Creating 'Company/Customer' entry in the company DB Table
	 * @return void
	 * */
	public static void createOwner( Owner owner, String ownerTable ) throws OwnerSqlException {

		String query = " insert into "+ ownerTable +" () values (?,?,?,?)";
		PreparedStatement ps = null;
		Connection _conn = null;

		try {
			_conn = _pool.getConnection();
			_conn.setAutoCommit(false);
			ps = _conn.prepareStatement(query);
			ps.setInt(1, owner.getID());
			ps.setString(2, owner.getName());
			ps.setString(3, owner.getPassword());
			ps.setString(4, owner.getEmail());
			ps.execute();
			_conn.commit();
		} catch ( SQLException e ) {
			if (_conn != null) // check if we still have connection initialized and try rolling back
				try {
					_conn.rollback();
				} catch (SQLException e2) {
					throw new OwnerSqlException(e2.getMessage(), OwnerSqlException.SQL_ERROR);
				}
			throw new OwnerSqlException(e.getMessage(), OwnerSqlException.FAIL_TO_INSERT);
		} finally {
			try {
				if ( ps != null )
					ps.close();//by closing prepared statement "ps" - result set "rs" is closed automatically
				_conn.setAutoCommit(true);
			} catch (SQLException e) {
				throw new OwnerSqlException(e.getMessage(), OwnerSqlException.SQL_ERROR);
			}
			_pool.returnConnection(_conn);
		}
	}
	
	/**
	 * Removing 'Company/Customer' entry from the company DB Table
	 * @return void
	 * */
	public static void removeOwner( Owner owner, String ownerTable ) throws OwnerSqlException {
		
		String query = " delete from " + ownerTable + " where id=" + owner.getID() + " limit 1";
		Connection _conn = null;
		PreparedStatement ps = null;
		
		try {
			_conn = _pool.getConnection();
			_conn.setAutoCommit(false);
			ps = _conn.prepareStatement(query);
			ps.execute();
			_conn.commit();
		} catch ( SQLException e ) {
			if ( _conn != null )
				try {
					_conn.rollback();
				} catch (SQLException e2) {
					throw new OwnerSqlException(e2.getMessage(), OwnerSqlException.SQL_ERROR);
				}
			throw new OwnerSqlException(e.getMessage(), OwnerSqlException.REMOVE_FAILED);
		} finally {
			try {
				if ( ps != null )
					ps.close();
				_conn.setAutoCommit(true);
			} catch (SQLException e) {
				throw new OwnerSqlException(e.getMessage(), OwnerSqlException.SQL_ERROR);
			}
			_pool.returnConnection(_conn);
		}
	}
	
	/**
	 * Updating 'Company/Customer' Table's specific entry in the company DB table
	 * @return void
	 * */
	public static void updateOwner( Owner owner, String ownerTable ) throws OwnerSqlException {
		
		PreparedStatement ps = null;
		String query = " update "+ ownerTable +" set name=?, password=?, email=? where id=?";
		Connection _conn = null;

		try {
			_conn = _pool.getConnection();
			_conn.setAutoCommit(false);
			ps = _conn.prepareStatement(query);
			ps.setString(1, owner.getName());
			ps.setString(2, owner.getPassword());
			ps.setString(3, owner.getEmail());
			ps.setInt(4, owner.getID());
			ps.execute();
			_conn.commit();
		} catch (SQLException e) {
			if ( _conn != null )
				try {
					_conn.rollback();
				} catch (SQLException e2) {
					throw new OwnerSqlException(e2.getMessage(), OwnerSqlException.SQL_ERROR);
				}
			throw new OwnerSqlException(e.getMessage(), OwnerSqlException.UPDATE_FAILED);
		} finally {			
			try {
				if ( ps != null )
					ps.close();
				_conn.setAutoCommit(true);
			} catch (SQLException e) {
				throw new OwnerSqlException(e.getMessage(), OwnerSqlException.SQL_ERROR);
			}
			_pool.returnConnection(_conn);
		}
	}
	
	/**
	 * Requesting specific 'Company/Customer' entry from the customer DB Table using Customer ID
	 * @return {@link Owner}
	 * */
	public static Owner getOwner( int ownerID, String ownerTable, String ownerClassname ) throws OwnerSqlException, GenRunTimeException {
		if ( ownerClassname == null )
			return null;
		
		String query = "select * from " + ownerTable + " where id=" + ownerID;
		Owner owner = null;
		Statement st = null;
		ResultSet rs = null;
		Connection _conn = null;
		
		try {
			_conn = _pool.getConnection();
			st = _conn.createStatement();
			rs = st.executeQuery(query);
			if ( ! rs.next() )
				throw new SQLException("No owner with id " + ownerID + " found" );
			owner = _getOwnerInstance(ownerClassname);
			owner.setID(rs.getInt(1));
			owner.setName(rs.getString(2));
			owner.setPassword(rs.getString(3));
			owner.setEmail(rs.getString(4));
		} catch ( SQLException e ) {
			throw new OwnerSqlException(e.getMessage(), OwnerSqlException.SQL_ERROR);
		} finally {
			if ( st != null )
				try {
					st.close();
				} catch (SQLException e) {
					throw new OwnerSqlException(e.getMessage(), OwnerSqlException.SQL_ERROR);
				}
			_pool.returnConnection(_conn);
		}
		return owner;
	}

	/**
	 * Retrieving given Company/Customer by its name from customer DB table
	 * @return {@link Owner}
	 * */
	public static Owner getOwner( String name, String ownerTable, String ownerClassname ) throws OwnerSqlException, GenRunTimeException {
		if ( ownerClassname == null || ownerTable == null )
			return null;
		
		String query = "select * from " + ownerTable + " where name='" + name + "'";
		Owner owner = null;
		Statement st = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = _pool.getConnection();
			st = conn.createStatement();
			rs = st.executeQuery(query);
			if ( ! rs.next() )
				throw new SQLException("No owner with name " + name + " found" );
			owner = _getOwnerInstance(ownerClassname);
			owner.setID(rs.getInt(1));
			owner.setName(rs.getString(2));
			owner.setPassword(rs.getString(3));
			owner.setEmail(rs.getString(4));
		} catch ( SQLException e ) {
			throw new OwnerSqlException(e.getMessage(), OwnerSqlException.SQL_ERROR);
		} finally {
			if ( st != null )
				try {
					st.close();
				} catch (SQLException e) {
					throw new OwnerSqlException(e.getMessage(), OwnerSqlException.SQL_ERROR);
				}
			_pool.returnConnection(conn);
		}
		return owner;
	}

	/**
	 * Retrieves all available Customers/Companies from customer DB table
	 * @return {@link Owner} Collection
	 */
	public static Collection<Owner> getAllOwners( String ownerTable, String ownerClassname ) throws OwnerSqlException, GenRunTimeException {
		if ( ownerClassname == null || ownerTable == null )
			return null;
		
		Collection<Owner> owners = null;
		Statement st = null;
		ResultSet rs = null;
		Connection conn = null;
		String query = "select * from " + ownerTable;
		
		try {
			conn = _pool.getConnection();
			st = conn.createStatement();
			rs = st.executeQuery(query);
			owners = deserializeOwners(ownerClassname, rs);
		} catch (SQLException e) {
			throw new OwnerSqlException(e.getMessage(), OwnerSqlException.SQL_ERROR);
		} finally {
			if ( st != null )
				try {
					st.close();
				} catch (SQLException e) {
					throw new OwnerSqlException(e.getMessage(), OwnerSqlException.SQL_ERROR);
				}
			_pool.returnConnection(conn);
		}
		return owners;
	}
	
	/**
	 * Retrieves all coupons that belongs to any specific Company/Customer<br/>
	 * @return Collection of {@linkplain Coupon}
	 * */
	public static Collection<Coupon> getCoupons( int ownerID, String couponOwnerTable, String ownerIDColumn ) throws OwnerSqlException {
		if ( ownerID <= 0 )
			return null;

		Collection<Coupon> coupons = null;
		String qurey = " select "+  SqlDB.COUPON_TABLE +".* from "+ couponOwnerTable
				+ " inner join " + SqlDB.COUPON_TABLE
				+ " on coupon.id = " + couponOwnerTable + ".coupon_id"
				+ " where " + ownerIDColumn + "=" + ownerID;
		
		Statement st = null;
		ResultSet rs = null;
		Connection _conn = null;

		try {
			_conn = _pool.getConnection();
			st = _conn.createStatement();
			rs = st.executeQuery(qurey);
			coupons = _deserializeCoupons( rs );
		} catch (SQLException e) {
			throw new OwnerSqlException(e.getMessage(), OwnerSqlException.SQL_ERROR);
		} finally {
			if ( st != null )
				try {
					st.close();
				} catch (SQLException e) {
					throw new OwnerSqlException(e.getMessage(), OwnerSqlException.SQL_ERROR);
				}
			_pool.returnConnection(_conn);
		}
		return coupons;
	}
	
	 /** Retrieves all inbox messages that are related to any specific Company/Customer by its id
	 * @return {@link Inbox} Collection
	 * */
	public static Collection<Inbox> getInboxMessages( int ownerID, String inboxOwnerTable, String ownerIDColumn ) throws OwnerSqlException {
		if ( ownerID <= 0 )
			return null;

		Collection<Inbox> inboxes = null;
		String qurey = " select "+  SqlDB.INBOX_TABLE +".* from "+ inboxOwnerTable 
				+ " inner join " + SqlDB.INBOX_TABLE
				+ " on inbox.id = " + inboxOwnerTable + ".inbox_id"
				+ " where " + ownerIDColumn + "=" + ownerID;

		Statement st = null;
		ResultSet rs = null;
		Connection _conn = null;

		try {
			_conn = _pool.getConnection();
			st = _conn.createStatement();
			rs = st.executeQuery(qurey);
			inboxes = _deserializeInboxes( rs );
		} catch (SQLException e) {
			throw new OwnerSqlException(e.getMessage(), OwnerSqlException.SQL_ERROR);
		} finally {
			if ( st != null )
				try {
					st.close();
				} catch (SQLException e) {
					throw new OwnerSqlException(e.getMessage(), OwnerSqlException.SQL_ERROR);
				}
			_pool.returnConnection(_conn);
		}
		return inboxes;
	}
	
	 /* This function is required to get the initial Company/Customer id number to start from, </br>
	 * for creating new IDs for coupons.
	 * @return int
	 * */
	public static int getOwnerLatestID( String ownerTable ) throws OwnerSqlException {
		int index = -1;
		Statement st = null;
		ResultSet rs = null;
		
		String query = "SELECT * FROM "+ ownerTable +" order by id DESC limit 1";
		Connection _conn = null;
		
		try {
			_conn = _pool.getConnection();
			st = _conn.createStatement();
			rs = st.executeQuery(query);
			if ( rs.next() )
				index = rs.getInt(1);
		} catch (SQLException e) {
			throw new OwnerSqlException(e.getMessage(), OwnerSqlException.SQL_ERROR);
		} finally {
			if ( st != null )
				try {
					st.close();
				} catch (SQLException e) {
					throw new OwnerSqlException(e.getMessage(), OwnerSqlException.SQL_ERROR);
				}
			_pool.returnConnection(_conn);
		}
		return index;
	}
	
	/**
	 * Login function verifies access previliges for any specific Company/Customer<br/>
	 * Returns true if credentials are valid - false otherwise
	 * @return boolean
	 * */
	public static boolean login( String ownerName, String password, String ownerTable ) throws OwnerSqlException {
		if ( ownerTable == null )
			return false;
		
		boolean permission = false;
		String query = " select password from " + ownerTable + " where name='" + ownerName + "'";
		Statement st = null;
		ResultSet rs = null;
		Connection _conn = null;
		
		try {
			_conn = _pool.getConnection();
			st = _conn.createStatement();
			rs = st.executeQuery(query);
			if ( rs.next() ) {
				if ( ! password.equals(rs.getString(1)) )
					throw new SQLException("Wrong username or password");
				permission = true;
			}
		} catch (SQLException e) {
			throw new OwnerSqlException(e.getMessage(), OwnerSqlException.SQL_ERROR);
		} finally {
			if ( st != null )
				try {
					st.close();
				} catch (SQLException e) {
					throw new OwnerSqlException(e.getMessage(), OwnerSqlException.SQL_ERROR);
				}
			_pool.returnConnection(_conn);
		}
		return permission;
	}
	
	
	/*============================================================*/
	
	/*
	 * Auxiliary private function that recreates Companies/Customers collection using ResultSet<br/>
	 * */
	private static Collection<Owner> deserializeOwners ( String ownerClassname, ResultSet rs ) throws SQLException, GenRunTimeException {
		if ( rs == null )
			return null;
		
		Owner owner = null;
		Collection<Owner> owners = new HashSet<>();
		while ( rs.next() ) {
			owner = _getOwnerInstance(ownerClassname);
			owner.setID(rs.getInt(1));
			owner.setName(rs.getString(2));
			owner.setPassword(rs.getString(3));
			owner.setEmail(rs.getString(4));
			owners.add(owner);
		}
		
		if ( owners.size() == 0 )
			return null;
		return owners;
	}
	
	/*
	 * Build dynamically Company/Customer instance based on classname recieved
	 * */
	private static Owner _getOwnerInstance( String ownerClassname ) {
		try {
			 return (Owner)Class.forName(ownerClassname).newInstance();
		} catch (InstantiationException  e) {
			throw new GenRunTimeException(e.getMessage(), GenRunTimeException.INSTANTIATION);
		} catch (IllegalAccessException e) {
			throw new GenRunTimeException(e.getMessage(), GenRunTimeException.ILLEGALACCESS);
		} catch (ClassNotFoundException e) {
			throw new GenRunTimeException(e.getMessage(), GenRunTimeException.CLASS_FOUND);
		}
	}
	
	 /* 
	  * Auxiliary private function that recreates coupons collection using ResultSet<br/>
	 * */
	private static Collection<Coupon> _deserializeCoupons( ResultSet rs ) throws SQLException {
		if ( rs == null )
			return null;
		
		Collection<Coupon> coupons = new HashSet<>();
		while ( rs.next() )
			coupons.add( new Coupon( rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getDate(4), 
					rs.getInt(5), CouponType.valueOf(rs.getString(6).toUpperCase()), rs.getString(7), 
					rs.getDouble(8), rs.getString(9) ) );
		
		return coupons;
	}
	
	 /* Auxiliary private function that recreates inbox collection using ResultSet<br/>
	 * */
	private static Collection<Inbox> _deserializeInboxes( ResultSet rs ) throws SQLException {
		if ( rs == null )
			return null;
		
		Collection<Inbox> inboxes = new HashSet<>();
		while ( rs.next() )
			inboxes.add( new Inbox( rs.getInt(1), rs.getString(2) ) );
		
		return inboxes;
	}
	


}
