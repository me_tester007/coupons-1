package com.project.daosql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.project.dao.LogDao;
import com.project.daofile.LogLevel;
import com.project.exceptions.sql.SqlRunTimeException;
import com.project.jdbc.ConnectionPool;
import com.project.logs.Log;



/************************************************************		
*															*
*		THIS CLASS IS DEPRECATED AND SHOULD NOT BE IN USE 	*
*															*
*************************************************************/
// Logs are written INTO FILES

/**
 * This is DataBase Layer Class working with MySQL DB server
 * represents the manipulation for log object
 * */
public class LogDBDao implements LogDao {

	private ConnectionPool _pool;

	public LogDBDao() {
		_pool = ConnectionPool.getInstance();
	}

	/**
	 * Creates log in the log db table
	 * @return void
	 * */
	@Override
	public void createLog( Log log, LogLevel logLevel ) throws SqlRunTimeException {
		if ( log == null )
			return;
		String query = " insert into logs () values " + log.getQuery();
		PreparedStatement ps = null;
		Connection _conn = null;

		try {
			_conn = _pool.getConnection();
			_conn.setAutoCommit(false);
			ps = _conn.prepareStatement(query);
			ps.execute();
			_conn.commit();
		} catch (SQLException e) {
			if ( _conn != null )
				try {
					_conn.rollback();
				} catch (SQLException e1) {
					throw new SqlRunTimeException(e1.getMessage(), SqlRunTimeException.SQL_ERROR);
				}
			throw new SqlRunTimeException(e.getMessage(), SqlRunTimeException.FAIL_TO_INSERT);
		} finally {
			try {
				if ( ps != null )
					ps.close();
				_conn.setAutoCommit(true);
			} catch (SQLException e) {
				throw new SqlRunTimeException(e.getMessage(), SqlRunTimeException.SQL_ERROR);
			}
			_pool.returnConnection(_conn);
		}
	}

	/**
	 * Updates log in the log db table
	 * @return void
	 * */
	@Override
	public void updateLog( Log log ) throws SqlRunTimeException {
		if ( log == null )
			return;

		String query = " update logs set timestamp=?, description=? where id=? ";
		PreparedStatement ps = null;
		Connection _conn = null;

		try {
			_conn = _pool.getConnection();
			_conn.setAutoCommit(false);
			ps = _conn.prepareStatement(query);
			ps.setDate(1, log.getDate());
			ps.setString(2, log.toString());
			ps.setInt(3, log.getID());
			ps.execute();
			_conn.commit();
		} catch (SQLException e) {
			if ( _conn != null )
				try {
					_conn.rollback();
				} catch (SQLException e1) {
					throw new SqlRunTimeException(e1.getMessage(), SqlRunTimeException.SQL_ERROR);
				}
			throw new SqlRunTimeException(e.getMessage(), SqlRunTimeException.UPDATE_FAILED);
		} finally {
			try {
				if ( ps != null )
					ps.close();
				_conn.setAutoCommit(true);
			} catch (SQLException e) {
				throw new SqlRunTimeException(e.getMessage(), SqlRunTimeException.SQL_ERROR);
			}
			_pool.returnConnection(_conn);
		}
	}

	/**
	 * Deletes log from the log db table
	 * @return void
	 * */
	@Override
	public void deleteLog( Log log ) throws SqlRunTimeException {
		if ( log == null )
			return;

		String query = " delete from logs where id=" + log.getID();
		PreparedStatement ps = null;
		Connection _conn = null;

		try {
			_conn = _pool.getConnection();
			_conn.setAutoCommit(false);
			ps = _conn.prepareStatement(query);
			ps.execute();
			_conn.commit();
		} catch (SQLException e) {
			if ( _conn != null )
				try {
					_conn.rollback();
				} catch (SQLException e1) {
					throw new SqlRunTimeException(e1.getMessage(), SqlRunTimeException.SQL_ERROR);
				}
			throw new SqlRunTimeException(e.getMessage(), SqlRunTimeException.REMOVE_FAILED);
		} finally {
			try {
				if ( ps != null )
					ps.close();
				_conn.setAutoCommit(true);
			} catch (SQLException e) {
				throw new SqlRunTimeException(e.getMessage(), SqlRunTimeException.SQL_ERROR);
			}
			_pool.returnConnection(_conn);
		}
	}

	/**
	 * this method is used to get latest index id of the log <br/>
	 * in order to continue id numbering
	 */
	public int getLogLatestID() throws SqlRunTimeException {
		int index = -1;
		Statement st = null;
		ResultSet rs = null;

		String query = "SELECT * FROM logs order by id DESC limit 1";
		Connection _conn = null;

		try {
			_conn = _pool.getConnection();
			st = _conn.createStatement();
			rs = st.executeQuery(query);
			if ( rs.next() )
				index = rs.getInt(1);
		} catch (SQLException e) {
			throw new SqlRunTimeException(e.getMessage(), SqlRunTimeException.SQL_ERROR);
		} finally {
			if ( st != null )
				try {
					st.close();
				} catch (SQLException e) {
					throw new SqlRunTimeException(e.getMessage(), SqlRunTimeException.SQL_ERROR);
				}
			_pool.returnConnection(_conn);
		}
		return index;
	}

}
